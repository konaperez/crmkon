<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new cfg\userBundle\cfguserBundle(),
            new start\loginBundle\startloginBundle(),
            new start\homeBundle\starthomeBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
<<<<<<< HEAD
            //new FM\ElfinderBundle\FMElfinderBundle(),
            //new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            //new freelancer\listaFreelancerBundle\listaFreelancerBundle(),
            new freelancer\registroFreelancerBundle\registroFreelancerBundle(),
=======
            new cfg\trBundle\cfgtrBundle(),
            new cfg\reportBundle\cfgreportBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
>>>>>>> 74762ff5c0e1cb0a04d6fefdcd527ad7c6b85b57
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();

            if ('dev' === $this->getEnvironment()) {
                $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
                $bundles[] = new Symfony\Bundle\WebServerBundle\WebServerBundle();
            }
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
