<?php

namespace freelancer\registroFreelancerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('registroFreelancerBundle:Default:index.html.twig');
    }
}
