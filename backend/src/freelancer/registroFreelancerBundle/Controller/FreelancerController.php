<?php

namespace freelancer\registroFreelancerBundle\Controller;

use freelancer\registroFreelancerBundle\Entity\Freelancer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


use freelancer\registroFreelancerBundle\Form\FreelancerType;

$request = Request::createFromGlobals();

class FreelancerController extends Controller
{
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();    
        $json =json_decode($_POST['json']);
        //$json =json_decode($_POST['json']);

            
        if($json->id != 0){
            $obj = $em->getRepository('registroFreelancerBundle:Freelancer')->find($json->id);
        }else{
            $obj = new Freelancer();
        }

        $obj->setNombre($json->nombre);
        $obj->setApellido($json->apellido);
        $obj->setRut($json->rut);
        $obj->setTelefono($json->telefono);
        $obj->setCorreo($json->correo);

        $validator = $this->get('validator');
        $errors = $validator->validate($obj);
        $array = array();
        
        if (count($errors) > 0) {
             foreach ($errors as $error) {
                 array_push($array, array($error->getPropertyPath() => $error->getMessage()));
             }
         }
 
         if(count($errors)==0){
 
             $em->persist($obj);
             $em->flush();
             return new JsonResponse(['success'=>'OK']);
             
         }
        return new JsonResponse(['permisos'=>'OK','errores'=>$array]);
        /*
        return $this->render('registroFreelancerBundle:Freelancer:add.html.twig',
            array(
                'freelancer' => $freelancer,
                'form'       => $form->createView(),
            ));
        /*
        return $this->render('registroFreelancerBundle:Freelancer:add.html.twig', array(
            // ...
        ));
        //*/
    }

    public function removeAction($id)
    {
        return $this->render('registroFreelancerBundle:Freelancer:remove.html.twig', array(
            // ...
        ));
    }

    public function editAction($id)
    {
        return $this->render('registroFreelancerBundle:Freelancer:edit.html.twig', array(
            // ...
        ));
    }

    public function listAction()
    {
        return $this->render('registroFreelancerBundle:Freelancer:list.html.twig', array(
            // ...
        ));
    }

    public function showAction(Freelancer $freelancer)
    {
       

        return $this->render('registroFreelancerBundle:Freelancer:show.html.twig', array(
            'freelancer' => $freelancer,
        ));
    }

    public function indexAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        $sql = $em->getRepository("registroFreelancerBundle:Freelancer");
        $sql = $sql->createQueryBuilder('m');
        
        $datos_mostrar=5;
        
        if($request->getMethod() == 'GET'){
        
        $data = $em->getRepository('registroFreelancerBundle:Freelancer')->findAll();
 
            return $this->render('registroFreelancerBundle:Freelancer:index.html.twig', array(
                'freelancers' => $data,
                // ...
            ));
        }else{
            
        $json =json_decode($_POST['json']); 
            
        $sql_count = $em->createQuery(" SELECT count(m) as cont
                        FROM registroFreelancerBundle:Freelancer m")->getArrayResult();
        $sql->orderBy("m.id", 'ASC');
        $sql->setFirstResult( $datos_mostrar * ( $json->page - 1)  );
        $sql->setMaxResults( $datos_mostrar );                       
        
        $freelancers = $sql->getQuery()->getArrayResult();
        //*/
            return new Response(json_encode(array(
                'res'=>true,
                'freelancers'    => $freelancers,
                'sql_count'=>$sql_count[0]['cont'],
                'page'=>$json->page
                )));//*/
        }
    }

        
    protected function getErrorsAsArray($form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error)
            $errors[] = $error->getMessage();
 
        foreach ($form->all() as $key => $child) {
            if ($err = $this->getErrorsAsArray($child))
                $errors[$key] = $err;
        }
        return $errors;
    }

    
}
