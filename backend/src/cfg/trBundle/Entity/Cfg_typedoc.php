<?php

namespace cfg\trBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Cfg_typedoc
 *
 * @ORM\Table(name="cfg_typedoc")
 * @ORM\Entity(repositoryClass="cfg\trBundle\Repository\Cfg_typedocRepository")
 */
class Cfg_typedoc
{  

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 10 caracteres"
     * )
     * @ORM\Column(name="abbr_typedoc", type="string", length=10, unique=true)
     */
    private $abbrTypedoc;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 255 caracteres"
     * )
     * @ORM\Column(name="name_typedoc", type="string", length=255, unique=true)
     */
    private $nameTypedoc;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="act_typedoc", type="string", length=2)
     */
    private $actTypedoc;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set abbrTypedoc.
     *
     * @param string $abbrTypedoc
     *
     * @return Cfg_typedoc
     */
    public function setAbbrTypedoc($abbrTypedoc)
    {
        $this->abbrTypedoc = $abbrTypedoc;

        return $this;
    }

    /**
     * Get abbrTypedoc.
     *
     * @return string
     */
    public function getAbbrTypedoc()
    {
        return $this->abbrTypedoc;
    }

    /**
     * Set nameTypedoc.
     *
     * @param string $nameTypedoc
     *
     * @return Cfg_typedoc
     */
    public function setNameTypedoc($nameTypedoc)
    {
        $this->nameTypedoc = $nameTypedoc;

        return $this;
    }

    /**
     * Get nameTypedoc.
     *
     * @return string
     */
    public function getNameTypedoc()
    {
        return $this->nameTypedoc;
    }

    /**
     * Set actTypedoc.
     *
     * @param string $actTypedoc
     *
     * @return Cfg_typedoc
     */
    public function setActTypedoc($actTypedoc)
    {
        $this->actTypedoc = $actTypedoc;

        return $this;
    }

    /**
     * Get actTypedoc.
     *
     * @return string
     */
    public function getActTypedoc()
    {
        return $this->actTypedoc;
    }
}
