<?php

namespace cfg\trBundle\Controller;

use cfg\trBundle\Entity\Cfg_typedoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cfg_typedoc controller.
 *
 */
class Cfg_typedocController extends Controller
{
    /**
     * Lists all cfg_typedoc entities.
     *
     */
    public function indexAction()
    {
        return new Response("Error");
    }

    /**
     * Creates a new cfg_typedoc entity.
     *
     */
    public function newAction(Request $request)
    {
        return new Response("Error");

    }

    /**
     * Finds and displays a cfg_typedoc entity.
     *
     */
    public function showAction(Cfg_typedoc $cfg_typedoc)
    {
        return new Response("Error");
    }

    /**
     * Displays a form to edit an existing cfg_typedoc entity.
     *
     */
    public function editAction(Request $request, Cfg_typedoc $cfg_typedoc)
    {
        return new Response("Error");
    }

    /**
     * Deletes a cfg_typedoc entity.
     *
     */
    public function deleteAction(Request $request, Cfg_typedoc $cfg_typedoc)
    {
        return new Response("Error");

    }

    /**
     * Creates a form to delete a cfg_typedoc entity.
     *
     * @param Cfg_typedoc $cfg_typedoc The cfg_typedoc entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cfg_typedoc $cfg_typedoc)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cfg_typedoc_delete', array('id' => $cfg_typedoc->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
