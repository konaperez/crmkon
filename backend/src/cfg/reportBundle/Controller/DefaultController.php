<?php

namespace cfg\reportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

//libreria para manegar liggio excel
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use PHPExcel_Cell;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use PHPExcel_Style_NumberFormat;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return new Response('Error');
    }
    public function entidadesAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $json =json_decode($_POST['json']);
        $tipo=$json->tipo;        
        $data = $em->createQuery(" SELECT m 
                FROM cfgreportBundle:Cfg_report m 
                WHERE m.tipoReport ='$tipo'")->getArrayResult();     
        return new JsonResponse(['data'=>array('tipo'=>$data)]);
        
    }
    
    public function generarAction(){
        $em = $this->getDoctrine()->getManager();
        $json =json_decode($_POST['json']);
        //se busca la consulta por el nombre de la consulta seleccionada
        $sql_data=$em->getRepository('cfgreportBundle:Cfg_report')->findOneBy(array('nameReport'=>$json->nameReport));

        $connection = $em->getConnection();
        $statement = $connection->prepare($sql_data->getSqlReport());
        if($statement->execute()){
            // se recogen todos los datos para posteriormente sacar el head para la tabla y el body
            $statement->execute();
            $results = $statement->fetchAll(); 
            $array=array();
            for($i=0;$i<sizeof($results);$i++){
                array_push($array,(array)$results[$i]);
            }
            return new JsonResponse(['success'=>'OK','head'=>array_keys($results[0]),'body'=>$array]); 
        }else{
            return new JsonResponse(['success'=>'ERROR']); 
        }
    
    }

    public function generarexcelAction(Request $request){

        set_time_limit( 0 );
        ini_set("memory_limit","1024M");
        $em = $this->getDoctrine()->getManager();
        //se busca la consulta por el nombre de la consulta seleccionada
        $sql_data=$em->getRepository('cfgreportBundle:Cfg_report')->findOneBy(array('nameReport'=>$_GET['id']));
        $connection = $em->getConnection();
        $statement = $connection->prepare($sql_data->getSqlReport()."BETWEEN '".$_GET['fin']."' AND '".$_GET['ffn']."'");
        //$statement = $connection->prepare($sql_data->getSqlReport());
        if($statement->execute()){
            
            $results = $statement->fetchAll();    
            if($results){
                $head = array_keys($results[0]);   
            }else{
                return new Response("No hay datos");
            }
        }else{
            return new Response("Error en la consulta");
        }
  


     

    
    
    
                  try{
                    /****************************************REPORTE***********************************/
            // recuperamos los elementos de base de datos que queremos exportar
            $session=new Session();
            $auto=$session->get('autopre');      
            // solicitamos el servicio 'phpexcel' y creamos el objeto vacío...
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
                    //First sheet
                    $sheet = $phpExcelObject->getActiveSheet();
                    //Start adding next sheets
                   
                    $objWorkSheet = $phpExcelObject->createSheet(0); //Setting index when creating
    
            $objWorkSheet->setTitle("Matriz Correlacion"); 
    

            $style = array(
                "font" => array("bold" => false,'size'=>7),           
                
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            $styleuno = array(
                "font" => array(
                    "bold" => true,
                    "color" => array("rgb" => "0000ff"),
                    'size'=>12
                )           
                ,'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            );
            


            $styledos = array(
                "font" => array(
                    "bold" => true,
                    "color" => array("rgb" => "#610B0B"),
                    'size'=>8
                )           
                ,'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            );

            $styletres = array(
                "font" => array(
                    "bold" => true,
                    "color" => array("rgb" => "#610B0B"),
                    'size'=>8
                )           
                ,'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
                'borders' => array(
                      'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THICK
                      )),
                    'fill' => array(
                  'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array(
                            'argb' => 'B6B6B6')
                  ),                                                                                                                
            );   

            $stylecuatro = array(
                "font" => array(
                    "bold" => false,
                    "color" => array("rgb" => "#610B0B"),
                    'size'=>8
                )           
                ,'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'wrap' => TRUE),
                'borders' => array(
                      'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN
                      )) ,                                                                                                           
            );     
            $style_colvariastres = array(
                "font" => array("bold" => false,"color" => array("rgb" => "FFFFFF"),'size'=>8),           
                'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
                'fill' => array('type'  => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'FF0000') )
                ); 
            $style_colvariasdos = array(
                "font" => array("bold" => false,"color" => array("rgb" => "000000"),'size'=>8),           
                'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
                'fill' => array('type'  => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'F78181') )
                ); 

            $col=1;
            foreach ($head as $t) {
                $StartcolumnIndex=PHPExcel_Cell::stringFromColumnIndex($col);
//                $objWorkSheet->getStyle($StartcolumnIndex)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objWorkSheet->setCellValue($StartcolumnIndex."2", strtoupper( $t), true)->getStyle()->applyFromArray($style_colvariasdos);
                $objWorkSheet->getColumnDimension($StartcolumnIndex)->setAutoSize(false);
                $objWorkSheet->getStyle($StartcolumnIndex)->getAlignment()->setWrapText(true);
                $objWorkSheet->getColumnDimension($StartcolumnIndex)->setWidth(20);                     
                $col++;
            }

                            //AJUSTAR TEXTO
                            $phpExcelObject->getActiveSheet()->getStyle('A1:E999')
                            ->getAlignment()->setWrapText(true); 
        
                        $fil=3;
                        foreach ($results as $r) {
                            $phpExcelObject->getActiveSheet()->getRowDimension($fil)->setRowHeight(25);                                                                                                                         

                            $col=1;
                            foreach($head as $t){
                                $StartcolumnIndex=PHPExcel_Cell::stringFromColumnIndex($col);
                                //$objWorkSheet->getStyle($StartcolumnIndex)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                                $objWorkSheet->setCellValue($StartcolumnIndex.$fil, strtoupper( $r[$t]), true)->getStyle()->applyFromArray($style);
                                $objWorkSheet->getColumnDimension($StartcolumnIndex)->setAutoSize(false);
                                //$objWorkSheet->getRowDimension(3)->setRowHeight(-1);    
                                                                                            
                                $objWorkSheet->getStyle($StartcolumnIndex)->getAlignment()->setWrapText(true);
                                $objWorkSheet->getColumnDimension($StartcolumnIndex)->setWidth(20);
                                $col++; 
                            }
                            $fil++;
                        }

                        
            // se crea el writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // se crea el response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // y por último se añaden las cabeceras
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'Reporte-'.date('Y-m-d').'.xls'
            );
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);
    
            return $response;
                    /****************************************REPORTE***********************************/
                    }catch(\Exception $ex){
    
                         return new Response("HA OCURRIDO UN ERROR AL GENERAR EL REPORTE. EXCEPTION: ".$ex->getMessage() );
                    }
    }

}
