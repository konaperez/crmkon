<?php

namespace cfg\reportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cfg_report
 *
 * @ORM\Table(name="cfg_report")
 * @ORM\Entity(repositoryClass="cfg\reportBundle\Repository\Cfg_reportRepository")
 */
class Cfg_report
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_report", type="string", length=255, unique=true)
     */
    private $nameReport;

    /**
     * @var string
     *
     * @ORM\Column(name="sql_report", type="text")
     */
    private $sqlReport;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_report", type="string", length=255)
     */
    private $tipoReport;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameReport.
     *
     * @param string $nameReport
     *
     * @return Cfg_report
     */
    public function setNameReport($nameReport)
    {
        $this->nameReport = $nameReport;

        return $this;
    }

    /**
     * Get nameReport.
     *
     * @return string
     */
    public function getNameReport()
    {
        return $this->nameReport;
    }

    /**
     * Set sqlReport.
     *
     * @param string $sqlReport
     *
     * @return Cfg_report
     */
    public function setSqlReport($sqlReport)
    {
        $this->sqlReport = $sqlReport;

        return $this;
    }

    /**
     * Get sqlReport.
     *
     * @return string
     */
    public function getSqlReport()
    {
        return $this->sqlReport;
    }

    /**
     * Set tipoReport.
     *
     * @param string $tipoReport
     *
     * @return Cfg_report
     */
    public function setTipoReport($tipoReport)
    {
        $this->tipoReport = $tipoReport;

        return $this;
    }

    /**
     * Get tipoReport.
     *
     * @return string
     */
    public function getTipoReport()
    {
        return $this->tipoReport;
    }
}
