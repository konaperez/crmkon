<?php
namespace cfg\userBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('getName', array($this, 'getNameFilter')),
        );
    }

    public function getNameFilter($name){
        switch ($name) {
            case "gerente":
                return "Gerente";
            
            case "revisorf":
                return "Revisor Fiscal";

            case "administrador":
                return "Administrador";

            case "contador":
                return "Contador";

            case "responsablep":
                return "Responsable Presupuesto";

            case "jefen":
                return "Jefe Nómina";

            case "tesorerop":
                return "Tesorero Pagador";
        }
    }

    public function getName()
    {
        return 'app_extension';
    }
}