<?php

namespace cfg\userBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Cfg_role
 *
 * @ORM\Table(name="cfg_role")
 * @ORM\Entity(repositoryClass="cfg\userBundle\Repository\Cfg_roleRepository")
 * @UniqueEntity(
 *     fields={"nameRole"},
 *     message="Este dato ya esta registrado"
 * ) 
 */
class Cfg_role
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 255 caracteres"
     * )
     * @ORM\Column(name="name_role", type="string", length=255, unique=true)
     */
    private $nameRole;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="act_role", type="string", length=2,nullable=true)
     */
    private $actRole;    



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rolePermission = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameRole
     *
     * @param string $nameRole
     *
     * @return Cfg_role
     */
    public function setNameRole($nameRole)
    {
        $this->nameRole = $nameRole;

        return $this;
    }

    /**
     * Get nameRole
     *
     * @return string
     */
    public function getNameRole()
    {
        return $this->nameRole;
    }

    /**
     * Add rolePermission
     *
     * @param \cfg\userBundle\Entity\Cfg_permission $rolePermission
     *
     * @return Cfg_role
     */
    public function addRolePermission(\cfg\userBundle\Entity\Cfg_permission $rolePermission)
    {
        $this->rolePermission[] = $rolePermission;

        return $this;
    }

    /**
     * Remove rolePermission
     *
     * @param \cfg\userBundle\Entity\Cfg_permission $rolePermission
     */
    public function removeRolePermission(\cfg\userBundle\Entity\Cfg_permission $rolePermission)
    {
        $this->rolePermission->removeElement($rolePermission);
    }

    /**
     * Get rolePermission
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRolePermission()
    {
        return $this->rolePermission;
    }

    /**
     * Add roleUser
     *
     * @param \cfg\userBundle\Entity\Cfg_user $roleUser
     *
     * @return Cfg_role
     */
    public function addRoleUser(\cfg\userBundle\Entity\Cfg_user $roleUser)
    {
        $this->roleUser[] = $roleUser;

        return $this;
    }

    /**
     * Remove roleUser
     *
     * @param \cfg\userBundle\Entity\Cfg_user $roleUser
     */
    public function removeRoleUser(\cfg\userBundle\Entity\Cfg_user $roleUser)
    {
        $this->roleUser->removeElement($roleUser);
    }

    /**
     * Get roleUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoleUser()
    {
        return $this->roleUser;
    }

    /**
     * Set actRole
     *
     * @param string $actRole
     *
     * @return Cfg_role
     */
    public function setActRole($actRole)
    {
        $this->actRole = $actRole;

        return $this;
    }

    /**
     * Get actRole
     *
     * @return string
     */
    public function getActRole()
    {
        return $this->actRole;
    }
}
