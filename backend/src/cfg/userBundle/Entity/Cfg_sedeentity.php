<?php

namespace cfg\userBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * User_sedeentity
 *
 * @ORM\Table(name="cfg_sedeentity")
 * @ORM\Entity(repositoryClass="cfg\userBundle\Repository\Cfg_sedeentityRepository")
 * @UniqueEntity(
 *     fields={"idSedeSedeentity","idEntitySedeentity"},
 *     message="Este dato ya esta registrado"
 * ) 
 */
class Cfg_sedeentity
{

    /**
     * @ORM\ManyToOne(targetEntity="cfg\userBundle\Entity\Cfg_sede")
     * @ORM\JoinColumn(name="id_sede_sedeentity", referencedColumnName="id",nullable=true)
     */
    private $idSedeSedeentity;

    /**
     * @ORM\ManyToOne(targetEntity="cfg\userBundle\Entity\Cfg_entity")
     * @ORM\JoinColumn(name="id_entity_sedeentity", referencedColumnName="id",nullable=true)
     */
    private $idEntitySedeentity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSedeSedeentity
     *
     * @param \cfg\userBundle\Entity\Cfg_sede $idSedeSedeentity
     *
     * @return Cfg_sedeentity
     */
    public function setIdSedeSedeentity(\cfg\userBundle\Entity\Cfg_sede $idSedeSedeentity = null)
    {
        $this->idSedeSedeentity = $idSedeSedeentity;

        return $this;
    }

    /**
     * Get idSedeSedeentity
     *
     * @return \cfg\userBundle\Entity\Cfg_sede
     */
    public function getIdSedeSedeentity()
    {
        return $this->idSedeSedeentity;
    }

    /**
     * Set idEntitySedeentity
     *
     * @param \cfg\userBundle\Entity\Cfg_entity $idEntitySedeentity
     *
     * @return Cfg_sedeentity
     */
    public function setIdEntitySedeentity(\cfg\userBundle\Entity\Cfg_entity $idEntitySedeentity = null)
    {
        $this->idEntitySedeentity = $idEntitySedeentity;

        return $this;
    }

    /**
     * Get idEntitySedeentity
     *
     * @return \cfg\userBundle\Entity\Cfg_entity
     */
    public function getIdEntitySedeentity()
    {
        return $this->idEntitySedeentity;
    }
}
