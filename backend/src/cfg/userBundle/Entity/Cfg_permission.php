<?php

namespace cfg\userBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Cfg_permission
 *
 * @ORM\Table(name="cfg_permission")
 * @ORM\Entity(repositoryClass="cfg\userBundle\Repository\Cfg_permissionRepository")
 */
class Cfg_permission
{

    /**
     * @ORM\ManyToOne(targetEntity="cfg\userBundle\Entity\Cfg_url")
     * @ORM\JoinColumn(name="id_url_permission", referencedColumnName="id",nullable=true)
     */
    private $idUrlPermission;

    /**
     * @ORM\ManyToOne(targetEntity="cfg\userBundle\Entity\Cfg_role")
     * @ORM\JoinColumn(name="id_role_permission", referencedColumnName="id",nullable=true)
     */
    private $idRolePermission;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="new_permission", type="string", length=2, options={"default" : "NO"},nullable=true)
     */
    private $newPermission='NO';

    /**
     * @var string
     * @ORM\Column(name="delete_permission", type="string", length=2, options={"default" : "NO"},nullable=true)
     */
    private $deletePermission='NO';

    /**
     * @var string
     * @ORM\Column(name="list_permission", type="string", length=2, options={"default" : "NO"},nullable=true)
     */
    private $listPermission='NO';

    /**
     * @var string
     * @ORM\Column(name="edit_permission", type="string", length=2, options={"default" : "NO"},nullable=true)
     */
    private $editPermission='NO';

    /**
     * @var string
     * @ORM\Column(name="show_permission", type="string", length=2, options={"default" : "NO"},nullable=true)
     */
    private $showPermission='NO';

    /**
     * @var string
     * @ORM\Column(name="cause_permission", type="string", length=2, options={"default" : "NO"},nullable=true)
     */
    private $causePermission='NO';

    /**
     * @var string
     * @ORM\Column(name="descause_permission", type="string", length=2, options={"default" : "NO"},nullable=true)
     */
    private $descausePermission='NO';

    /**
     * @var string
     * @ORM\Column(name="visible_permission", type="string", length=2, options={"default" : "NO"},nullable=true)
     */
    private $visiblePermission='NO';    






    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set newPermission
     *
     * @param string $newPermission
     *
     * @return Cfg_permission
     */
    public function setNewPermission($newPermission)
    {
        if($newPermission===null || $newPermission==''){
            $this->newPermission = 'NO';
        }else{
            $this->newPermission = $newPermission;
        }

        return $this;
    }

    /**
     * Get newPermission
     *
     * @return string
     */
    public function getNewPermission()
    {
        return $this->newPermission;
    }

    /**
     * Set deletePermission
     *
     * @param string $deletePermission
     *
     * @return Cfg_permission
     */
    public function setDeletePermission($deletePermission)
    {
        if($deletePermission==null){
            $this->deletePermission = 'NO';
        }else{
            $this->deletePermission = $deletePermission;
        }        

        return $this;
    }

    /**
     * Get deletePermission
     *
     * @return string
     */
    public function getDeletePermission()
    {
        return $this->deletePermission;
    }

    /**
     * Set listPermission
     *
     * @param string $listPermission
     *
     * @return Cfg_permission
     */
    public function setListPermission($listPermission)
    {
        if($listPermission==null){
            $this->listPermission = 'NO';
        }else{
            $this->listPermission = $listPermission;
        }          

        return $this;
    }

    /**
     * Get listPermission
     *
     * @return string
     */
    public function getListPermission()
    {
        return $this->listPermission;
    }

    /**
     * Set editPermission
     *
     * @param string $editPermission
     *
     * @return Cfg_permission
     */
    public function setEditPermission($editPermission)
    {
        if($editPermission==null){
            $this->editPermission = 'NO';
        }else{
            $this->editPermission = $editPermission;
        }         

        return $this;
    }

    /**
     * Get editPermission
     *
     * @return string
     */
    public function getEditPermission()
    {
        return $this->editPermission;
    }

    /**
     * Set showPermission
     *
     * @param string $showPermission
     *
     * @return Cfg_permission
     */
    public function setShowPermission($showPermission)
    {
        if($showPermission==null){
            $this->showPermission = 'NO';
        }else{
            $this->showPermission = $showPermission;
        }         

        return $this;
    }

    /**
     * Get showPermission
     *
     * @return string
     */
    public function getShowPermission()
    {
        return $this->showPermission;
    }

    /**
     * Set causePermission
     *
     * @param string $causePermission
     *
     * @return Cfg_permission
     */
    public function setCausePermission($causePermission)
    {
        if($causePermission==null){
            $this->causePermission = 'NO';
        }else{
            $this->causePermission = $causePermission;
        }          

        return $this;
    }

    /**
     * Get causePermission
     *
     * @return string
     */
    public function getCausePermission()
    {
        return $this->causePermission;
    }

    /**
     * Set descausePermission
     *
     * @param string $descausePermission
     *
     * @return Cfg_permission
     */
    public function setDescausePermission($descausePermission)
    {
        if($descausePermission==null){
            $this->descausePermission = 'NO';
        }else{
            $this->descausePermission = $descausePermission;
        }        

        return $this;
    }

    /**
     * Get descausePermission
     *
     * @return string
     */
    public function getDescausePermission()
    {
        return $this->descausePermission;
    }

    /**
     * Set visiblePermission
     *
     * @param string $visiblePermission
     *
     * @return Cfg_permission
     */
    public function setVisiblePermission($visiblePermission)
    {
        if($visiblePermission==null){
            $this->visiblePermission = 'NO';
        }else{
            $this->visiblePermission = $visiblePermission;
        }         

        return $this;
    }

    /**
     * Get visiblePermission
     *
     * @return string
     */
    public function getVisiblePermission()
    {
        return $this->visiblePermission;
    }

    /**
     * Set idUrlPermission
     *
     * @param \cfg\userBundle\Entity\Cfg_url $idUrlPermission
     *
     * @return Cfg_permission
     */
    public function setIdUrlPermission(\cfg\userBundle\Entity\Cfg_url $idUrlPermission = null)
    {
        $this->idUrlPermission = $idUrlPermission;

        return $this;
    }

    /**
     * Get idUrlPermission
     *
     * @return \cfg\userBundle\Entity\Cfg_url
     */
    public function getIdUrlPermission()
    {
        return $this->idUrlPermission;
    }

    /**
     * Set idRolePermission
     *
     * @param \cfg\userBundle\Entity\Cfg_role $idRolePermission
     *
     * @return Cfg_permission
     */
    public function setIdRolePermission(\cfg\userBundle\Entity\Cfg_role $idRolePermission = null)
    {
        $this->idRolePermission = $idRolePermission;

        return $this;
    }

    /**
     * Get idRolePermission
     *
     * @return \cfg\userBundle\Entity\Cfg_role
     */
    public function getIdRolePermission()
    {
        return $this->idRolePermission;
    }
}
