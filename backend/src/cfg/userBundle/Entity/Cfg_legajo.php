<?php

namespace cfg\userBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Cfg_legajo
 *
 * @ORM\Table(name="cfg_legajo")
 * @ORM\Entity(repositoryClass="cfg\userBundle\Repository\Cfg_legajoRepository")
 */
// Variable utilizada para el inicio de legajo identificador unico de usuario, si esta variable ya se ha registrado simplemente se actualiza al numero siguiente
define('LEGAJO','100000');

class Cfg_legajo
{
    /**
     * @ORM\ManyToOne(targetEntity="cfg\userBundle\Entity\Cfg_legajo")
     * @ORM\JoinColumn(name="id_user_legajo", referencedColumnName="id",nullable=true)
     */
    private $idUserLegajo; 

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="legajo_legajo", type="integer", unique=true,nullable=true)
     */
    private $legajoLegajo;

    /**
     * @var string
     *
     * @ORM\Column(name="cargo_legajo", type="string", length=255,nullable=true)
     */
    private $cargoLegajo;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set legajoLegajo.
     *
     * @param int $legajoLegajo
     *
     * @return Cfg_legajo
     */
    public function setLegajoLegajo($legajoLegajo)
    {

        return $this;
    }

    /**
     * Get legajoLegajo.
     *
     * @return int
     */
    public function getLegajoLegajo()
    {
        return $this->legajoLegajo;
    }

    /**
     * Set cargoLegajo.
     *
     * @param string $cargoLegajo
     *
     * @return Cfg_legajo
     */
    public function setCargoLegajo($cargoLegajo)
    {
        $this->cargoLegajo = $cargoLegajo;

        return $this;
    }

    /**
     * Get cargoLegajo.
     *
     * @return string
     */
    public function getCargoLegajo()
    {
        return $this->cargoLegajo;
    }

    /**
     * Set idUserLegajo.
     *
     * @param \cfg\userBundle\Entity\Cfg_legajo|null $idUserLegajo
     *
     * @return Cfg_legajo
     */
    public function setIdUserLegajo(\cfg\userBundle\Entity\Cfg_legajo $idUserLegajo = null)
    {
        $this->idUserLegajo = $idUserLegajo;

        return $this;
    }

    /**
     * Get idUserLegajo.
     *
     * @return \cfg\userBundle\Entity\Cfg_legajo|null
     */
    public function getIdUserLegajo()
    {
        return $this->idUserLegajo;
    }
}
