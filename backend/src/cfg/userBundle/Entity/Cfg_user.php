<?php

namespace cfg\userBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Cfg_user
 *
 * @ORM\Table(name="cfg_user")
 * @ORM\Entity(repositoryClass="cfg\userBundle\Repository\Cfg_userRepository")
 */
class Cfg_user
{

    /**
     * @ORM\ManyToOne(targetEntity="cfg\userBundle\Entity\Cfg_entity")
     * @ORM\JoinColumn(name="id_entity_user", referencedColumnName="id",nullable=true)
     */
    private $idEntityUser;

    /**
     * @ORM\ManyToOne(targetEntity="cfg\userBundle\Entity\Cfg_role")
     * @ORM\JoinColumn(name="id_role_user", referencedColumnName="id",nullable=true)
     */
    private $idRoleUser;
    
    /**
     * @ORM\ManyToOne(targetEntity="cfg\trBundle\Entity\Cfg_typedoc")
     * @ORM\JoinColumn(name="id_typedoc_user", referencedColumnName="id",nullable=true)
     */
    private $idTypedocUser;    



    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 255 caracteres"
     * )
     * @ORM\Column(name="name_user", type="string", length=255, unique=true)
     */
    private $nameUser;


    /**
     * @var string
     * @ORM\Column(name="img_user", type="string", length=255, nullable=true)
     */
    private $imgUser;


    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 6,
     *      minMessage = "Este dato debe ser mayor a 6 carácteres",
     * )
     * @ORM\Column(name="passwd_user", type="string", length=255)
     */
    private $passwdUser;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")  
     * @ORM\Column(name="email_user", type="string", length=255, unique=true)
     */
    private $emailUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createon_user", type="datetime",nullable =true)
     */
    private $createonUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updateon_user", type="datetime",nullable =true)
     */
    private $updateonUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastaccess_user", type="datetime",nullable =true)
     */
    private $lastaccessUser;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="act_user", type="string", length=2)
     */
    private $actUser;

    /**
     * @var string
     * @ORM\Column(name="type_user", type="string", length=50, nullable=true)
     */
    private $typeUser;    


    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 255 caracteres"
     * )
     * @ORM\Column(name="nameape_user", type="string", length=255, nullable=true)
     */
    private $nameapeUser;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 255 caracteres"
     * )
     * @ORM\Column(name="numid_user", type="string", length=255, nullable=true)
     */
    private $numidUser;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="typeloguin_user", type="string", length=10, nullable=true)
     */
    private $typeloguinUser;    


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userVenta = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userPermissionesp = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameUser
     *
     * @param string $nameUser
     *
     * @return Cfg_user
     */
    public function setNameUser($nameUser)
    {
        $this->nameUser = $nameUser;

        return $this;
    }

    /**
     * Get nameUser
     *
     * @return string
     */
    public function getNameUser()
    {
        return $this->nameUser;
    }

    /**
     * Set imgUser
     *
     * @param string $imgUser
     *
     * @return Cfg_user
     */
    public function setImgUser($imgUser)
    {
        $this->imgUser = $imgUser;

        return $this;
    }

    /**
     * Get imgUser
     *
     * @return string
     */
    public function getImgUser()
    {
        return $this->imgUser;
    }

    /**
     * Set passwdUser
     *
     * @param string $passwdUser
     *
     * @return Cfg_user
     */
    public function setPasswdUser($passwdUser)
    {
        $this->passwdUser = $passwdUser;

        return $this;
    }

    /**
     * Get passwdUser
     *
     * @return string
     */
    public function getPasswdUser()
    {
        return $this->passwdUser;
    }

    /**
     * Set emailUser
     *
     * @param string $emailUser
     *
     * @return Cfg_user
     */
    public function setEmailUser($emailUser)
    {
        $this->emailUser = $emailUser;

        return $this;
    }

    /**
     * Get emailUser
     *
     * @return string
     */
    public function getEmailUser()
    {
        return $this->emailUser;
    }

    /**
     * Set createonUser
     *
     * @param \DateTime $createonUser
     *
     * @return Cfg_user
     */
    public function setCreateonUser($createonUser)
    {
        $this->createonUser = $createonUser;

        return $this;
    }

    /**
     * Get createonUser
     *
     * @return \DateTime
     */
    public function getCreateonUser()
    {
        return $this->createonUser;
    }

    /**
     * Set updateonUser
     *
     * @param \DateTime $updateonUser
     *
     * @return Cfg_user
     */
    public function setUpdateonUser($updateonUser)
    {
        $this->updateonUser = $updateonUser;

        return $this;
    }

    /**
     * Get updateonUser
     *
     * @return \DateTime
     */
    public function getUpdateonUser()
    {
        return $this->updateonUser;
    }

    /**
     * Set lastaccessUser
     *
     * @param \DateTime $lastaccessUser
     *
     * @return Cfg_user
     */
    public function setLastaccessUser($lastaccessUser)
    {
        $this->lastaccessUser = $lastaccessUser;

        return $this;
    }

    /**
     * Get lastaccessUser
     *
     * @return \DateTime
     */
    public function getLastaccessUser()
    {
        return $this->lastaccessUser;
    }

    /**
     * Set actUser
     *
     * @param string $actUser
     *
     * @return Cfg_user
     */
    public function setActUser($actUser)
    {
        $this->actUser = $actUser;

        return $this;
    }

    /**
     * Get actUser
     *
     * @return string
     */
    public function getActUser()
    {
        return $this->actUser;
    }

    /**
     * Set typeUser
     *
     * @param string $typeUser
     *
     * @return Cfg_user
     */
    public function setTypeUser($typeUser)
    {
        $this->typeUser = $typeUser;

        return $this;
    }

    /**
     * Get typeUser
     *
     * @return string
     */
    public function getTypeUser()
    {
        return $this->typeUser;
    }

    /**
     * Add userVentum
     *
     * @param \inv\invBundle\Entity\Inv_venta $userVentum
     *
     * @return Cfg_user
     */
    public function addUserVentum(\inv\invBundle\Entity\Inv_venta $userVentum)
    {
        $this->userVenta[] = $userVentum;

        return $this;
    }

    /**
     * Remove userVentum
     *
     * @param \inv\invBundle\Entity\Inv_venta $userVentum
     */
    public function removeUserVentum(\inv\invBundle\Entity\Inv_venta $userVentum)
    {
        $this->userVenta->removeElement($userVentum);
    }

    /**
     * Get userVenta
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserVenta()
    {
        return $this->userVenta;
    }

    /**
     * Add userPermissionesp
     *
     * @param \cfg\userBundle\Entity\Cfg_permissionesp $userPermissionesp
     *
     * @return Cfg_user
     */
    public function addUserPermissionesp(\cfg\userBundle\Entity\Cfg_permissionesp $userPermissionesp)
    {
        $this->userPermissionesp[] = $userPermissionesp;

        return $this;
    }

    /**
     * Remove userPermissionesp
     *
     * @param \cfg\userBundle\Entity\Cfg_permissionesp $userPermissionesp
     */
    public function removeUserPermissionesp(\cfg\userBundle\Entity\Cfg_permissionesp $userPermissionesp)
    {
        $this->userPermissionesp->removeElement($userPermissionesp);
    }

    /**
     * Get userPermissionesp
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserPermissionesp()
    {
        return $this->userPermissionesp;
    }

    /**
     * Set idEntityUser
     *
     * @param \cfg\userBundle\Entity\Cfg_entity $idEntityUser
     *
     * @return Cfg_user
     */
    public function setIdEntityUser(\cfg\userBundle\Entity\Cfg_entity $idEntityUser = null)
    {
        $this->idEntityUser = $idEntityUser;

        return $this;
    }

    /**
     * Get idEntityUser
     *
     * @return \cfg\userBundle\Entity\Cfg_entity
     */
    public function getIdEntityUser()
    {
        return $this->idEntityUser;
    }

    /**
     * Set idRoleUser
     *
     * @param \cfg\userBundle\Entity\Cfg_role $idRoleUser
     *
     * @return Cfg_user
     */
    public function setIdRoleUser(\cfg\userBundle\Entity\Cfg_role $idRoleUser = null)
    {
        $this->idRoleUser = $idRoleUser;

        return $this;
    }

    /**
     * Get idRoleUser
     *
     * @return \cfg\userBundle\Entity\Cfg_role
     */
    public function getIdRoleUser()
    {
        return $this->idRoleUser;
    }

    /**
     * Set nameapeUser
     *
     * @param string $nameapeUser
     *
     * @return Cfg_user
     */
    public function setNameapeUser($nameapeUser)
    {
        $this->nameapeUser = $nameapeUser;

        return $this;
    }

    /**
     * Get nameapeUser
     *
     * @return string
     */
    public function getNameapeUser()
    {
        return $this->nameapeUser;
    }

    /**
     * Set numidUser.
     *
     * @param string|null $numidUser
     *
     * @return Cfg_user
     */
    public function setNumidUser($numidUser = null)
    {
        $this->numidUser = $numidUser;

        return $this;
    }

    /**
     * Get numidUser.
     *
     * @return string|null
     */
    public function getNumidUser()
    {
        return $this->numidUser;
    }

    /**
     * Set idTypedocUser.
     *
     * @param \cfg\trBundle\Entity\Cfg_typedoc|null $idTypedocUser
     *
     * @return Cfg_user
     */
    public function setIdTypedocUser(\cfg\trBundle\Entity\Cfg_typedoc $idTypedocUser = null)
    {
        $this->idTypedocUser = $idTypedocUser;

        return $this;
    }

    /**
     * Get idTypedocUser.
     *
     * @return \cfg\trBundle\Entity\Cfg_typedoc|null
     */
    public function getIdTypedocUser()
    {
        return $this->idTypedocUser;
    }

    /**
     * Set typeloguinUser.
     *
     * @param string|null $typeloguinUser
     *
     * @return Cfg_user
     */
    public function setTypeloguinUser($typeloguinUser = null)
    {
        $this->typeloguinUser = $typeloguinUser;

        return $this;
    }

    /**
     * Get typeloguinUser.
     *
     * @return string|null
     */
    public function getTypeloguinUser()
    {
        return $this->typeloguinUser;
    }
}
