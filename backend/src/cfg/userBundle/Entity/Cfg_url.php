<?php

namespace cfg\userBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cfg_url
 *
 * @ORM\Table(name="cfg_url")
 * @ORM\Entity(repositoryClass="cfg\userBundle\Repository\Cfg_urlRepository")
 */
class Cfg_url
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="path_url", type="string", length=255, unique=true)
     */
    private $pathUrl;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="esmodule_url", type="string", length=2)
     */
    private $esmoduleUrl;

    /**
     * @var int
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="obj_url", type="integer")
     */
    private $objUrl;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")     
     * @ORM\Column(name="nameaccess_url", type="string", length=255, unique=true)
     */
    private $nameaccessUrl;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="name_url", type="string", length=255)
     */
    private $nameUrl;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="icon_url", type="string", length=255)
     */
    private $iconUrl;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="color_url", type="string", length=255)
     */
    private $colorUrl;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="act_url", type="string", length=2)
     */
    private $actUrl;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="namepadre_url", type="string", length=255)
     */
    private $namepadreUrl;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pathUrl
     *
     * @param string $pathUrl
     *
     * @return Cfg_url
     */
    public function setPathUrl($pathUrl)
    {
        $this->pathUrl = $pathUrl;

        return $this;
    }

    /**
     * Get pathUrl
     *
     * @return string
     */
    public function getPathUrl()
    {
        return $this->pathUrl;
    }

    /**
     * Set esmoduleUrl
     *
     * @param string $esmoduleUrl
     *
     * @return Cfg_url
     */
    public function setEsmoduleUrl($esmoduleUrl)
    {
        $this->esmoduleUrl = $esmoduleUrl;

        return $this;
    }

    /**
     * Get esmoduleUrl
     *
     * @return string
     */
    public function getEsmoduleUrl()
    {
        return $this->esmoduleUrl;
    }

    /**
     * Set objUrl
     *
     * @param integer $objUrl
     *
     * @return Cfg_url
     */
    public function setObjUrl($objUrl)
    {
        $this->objUrl = $objUrl;

        return $this;
    }

    /**
     * Get objUrl
     *
     * @return int
     */
    public function getObjUrl()
    {
        return $this->objUrl;
    }

    /**
     * Set nameaccessUrl
     *
     * @param string $nameaccessUrl
     *
     * @return Cfg_url
     */
    public function setNameaccessUrl($nameaccessUrl)
    {
        $this->nameaccessUrl = $nameaccessUrl;

        return $this;
    }

    /**
     * Get nameaccessUrl
     *
     * @return string
     */
    public function getNameaccessUrl()
    {
        return $this->nameaccessUrl;
    }

    /**
     * Set nameUrl
     *
     * @param string $nameUrl
     *
     * @return Cfg_url
     */
    public function setNameUrl($nameUrl)
    {
        $this->nameUrl = $nameUrl;

        return $this;
    }

    /**
     * Get nameUrl
     *
     * @return string
     */
    public function getNameUrl()
    {
        return $this->nameUrl;
    }

    /**
     * Set iconUrl
     *
     * @param string $iconUrl
     *
     * @return Cfg_url
     */
    public function setIconUrl($iconUrl)
    {
        $this->iconUrl = $iconUrl;

        return $this;
    }

    /**
     * Get iconUrl
     *
     * @return string
     */
    public function getIconUrl()
    {
        return $this->iconUrl;
    }

    /**
     * Set colorUrl
     *
     * @param string $colorUrl
     *
     * @return Cfg_url
     */
    public function setColorUrl($colorUrl)
    {
        $this->colorUrl = $colorUrl;

        return $this;
    }

    /**
     * Get colorUrl
     *
     * @return string
     */
    public function getColorUrl()
    {
        return $this->colorUrl;
    }

    /**
     * Set actUrl
     *
     * @param string $actUrl
     *
     * @return Cfg_url
     */
    public function setActUrl($actUrl)
    {
        $this->actUrl = $actUrl;

        return $this;
    }

    /**
     * Get actUrl
     *
     * @return string
     */
    public function getActUrl()
    {
        return $this->actUrl;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->urlPermission = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add urlPermission
     *
     * @param \cfg\userBundle\Entity\Cfg_permission $urlPermission
     *
     * @return Cfg_url
     */
    public function addUrlPermission(\cfg\userBundle\Entity\Cfg_permission $urlPermission)
    {
        $this->urlPermission[] = $urlPermission;

        return $this;
    }

    /**
     * Remove urlPermission
     *
     * @param \cfg\userBundle\Entity\Cfg_permission $urlPermission
     */
    public function removeUrlPermission(\cfg\userBundle\Entity\Cfg_permission $urlPermission)
    {
        $this->urlPermission->removeElement($urlPermission);
    }

    /**
     * Get urlPermission
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUrlPermission()
    {
        return $this->urlPermission;
    }

    /**
     * Set namepadreUrl
     *
     * @param string $namepadreUrl
     *
     * @return Cfg_url
     */
    public function setNamepadreUrl($namepadreUrl)
    {
        $this->namepadreUrl = $namepadreUrl;

        return $this;
    }

    /**
     * Get namepadreUrl
     *
     * @return string
     */
    public function getNamepadreUrl()
    {
        return $this->namepadreUrl;
    }
}
