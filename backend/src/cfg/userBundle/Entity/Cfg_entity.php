<?php

namespace cfg\userBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cfg_entity
 *
 * @ORM\Table(name="cfg_entity")
 * @ORM\Entity(repositoryClass="cfg\userBundle\Repository\Cfg_entityRepository")
 */
class Cfg_entity
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
    * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 255 caracteres"
     * )
     * @ORM\Column(name="name_entity", type="string", length=255, unique=true)
     */
    private $nameEntity;

    /**
     * @var string
    * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="address_entity", type="string", length=255)
     */
    private $addressEntity;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="phone_entity", type="string", length=255)
     */
    private $phoneEntity;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es correcto.",
     *     checkMX = true
     * )        
     * @ORM\Column(name="email_entity", type="string", length=255)
     */
    private $emailEntity;

    /**
     * @var string
     * @ORM\Column(name="obs_entity", type="string", length=255,nullable=true)
     */
    private $obsEntity;

    /**
     * @var string
     * @ORM\Column(name="web_entity", type="string", length=255,nullable=true)
     */
    private $webEntity;

    /**
     * @var \DateTime
     * @ORM\Column(name="createon_entity", type="datetime",nullable=true)
     */
    private $createonEntity;

    /**
     * @var \DateTime
     * @ORM\Column(name="updateon_entity", type="datetime",nullable=true)
     */
    private $updateonEntity;



    /**
     * @var string
     * @ORM\Column(name="city_entity", type="string", length=255, nullable=true)
     */
    private $cityEntity;

    /**
     * @var string
     * @ORM\Column(name="departamento_entity", type="string", length=255, nullable=true)
     */
    private $departamentoEntity;  

    /**
     * @var string
     * @ORM\Column(name="img_entity", type="string", length=255, nullable=true)
     */
    private $imgEntity; 

    /**
     * @var string
     * @ORM\Column(name="act_entity", type="string", length=2, nullable=true)
     */
    private $actEntity;

    /**
     * @var string
     * @ORM\Column(name="sede_entity", type="string", length=255, nullable=true)
     */
    private $sedeEntity;    

    /**
     * @var string
     * @ORM\Column(name="ident_entity", type="string", length=10, nullable=true)
     */
    private $identEntity; 
      


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entitySedeentity = new \Doctrine\Common\Collections\ArrayCollection();
        $this->entityUser = new \Doctrine\Common\Collections\ArrayCollection();
    }

public function getFoto($imgEntity)
{
    return $this->imgEntity;
}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameEntity
     *
     * @param string $nameEntity
     *
     * @return Cfg_entity
     */
    public function setNameEntity($nameEntity)
    {
        $this->nameEntity = $nameEntity;

        return $this;
    }

    /**
     * Get nameEntity
     *
     * @return string
     */
    public function getNameEntity()
    {
        return $this->nameEntity;
    }

    /**
     * Set addressEntity
     *
     * @param string $addressEntity
     *
     * @return Cfg_entity
     */
    public function setAddressEntity($addressEntity)
    {
        $this->addressEntity = $addressEntity;

        return $this;
    }

    /**
     * Get addressEntity
     *
     * @return string
     */
    public function getAddressEntity()
    {
        return $this->addressEntity;
    }

    /**
     * Set phoneEntity
     *
     * @param string $phoneEntity
     *
     * @return Cfg_entity
     */
    public function setPhoneEntity($phoneEntity)
    {
        $this->phoneEntity = $phoneEntity;

        return $this;
    }

    /**
     * Get phoneEntity
     *
     * @return string
     */
    public function getPhoneEntity()
    {
        return $this->phoneEntity;
    }

    /**
     * Set emailEntity
     *
     * @param string $emailEntity
     *
     * @return Cfg_entity
     */
    public function setEmailEntity($emailEntity)
    {
        $this->emailEntity = $emailEntity;

        return $this;
    }

    /**
     * Get emailEntity
     *
     * @return string
     */
    public function getEmailEntity()
    {
        return $this->emailEntity;
    }

    /**
     * Set obsEntity
     *
     * @param string $obsEntity
     *
     * @return Cfg_entity
     */
    public function setObsEntity($obsEntity)
    {
        $this->obsEntity = $obsEntity;

        return $this;
    }

    /**
     * Get obsEntity
     *
     * @return string
     */
    public function getObsEntity()
    {
        return $this->obsEntity;
    }

    /**
     * Set webEntity
     *
     * @param string $webEntity
     *
     * @return Cfg_entity
     */
    public function setWebEntity($webEntity)
    {
        $this->webEntity = $webEntity;

        return $this;
    }

    /**
     * Get webEntity
     *
     * @return string
     */
    public function getWebEntity()
    {
        return $this->webEntity;
    }

    /**
     * Set createonEntity
     *
     * @param \DateTime $createonEntity
     *
     * @return Cfg_entity
     */
    public function setCreateonEntity($createonEntity)
    {
        $this->createonEntity = $createonEntity;

        return $this;
    }

    /**
     * Get createonEntity
     *
     * @return \DateTime
     */
    public function getCreateonEntity()
    {
        return $this->createonEntity;
    }

    /**
     * Set updateonEntity
     *
     * @param \DateTime $updateonEntity
     *
     * @return Cfg_entity
     */
    public function setUpdateonEntity($updateonEntity)
    {
        $this->updateonEntity = $updateonEntity;

        return $this;
    }

    /**
     * Get updateonEntity
     *
     * @return \DateTime
     */
    public function getUpdateonEntity()
    {
        return $this->updateonEntity;
    }

    /**
     * Set cityEntity
     *
     * @param string $cityEntity
     *
     * @return Cfg_entity
     */
    public function setCityEntity($cityEntity)
    {
        $this->cityEntity = $cityEntity;

        return $this;
    }

    /**
     * Get cityEntity
     *
     * @return string
     */
    public function getCityEntity()
    {
        return $this->cityEntity;
    }

    /**
     * Set departamentoEntity
     *
     * @param string $departamentoEntity
     *
     * @return Cfg_entity
     */
    public function setDepartamentoEntity($departamentoEntity)
    {
        $this->departamentoEntity = $departamentoEntity;

        return $this;
    }

    /**
     * Get departamentoEntity
     *
     * @return string
     */
    public function getDepartamentoEntity()
    {
        return $this->departamentoEntity;
    }

    /**
     * Set imgEntity
     *
     * @param string $imgEntity
     *
     * @return Cfg_entity
     */
    public function setImgEntity($imgEntity)
    {
        $this->imgEntity = $imgEntity;

        return $this;
    }

    /**
     * Get imgEntity
     *
     * @return string
     */
    public function getImgEntity()
    {
        if($this->imgEntity != null ){
/*



*/
//header('Content-type: image/png'); 
       //return null;
       //return stream_get_contents($this->imgEntity);
        return $this->imgEntity;
        }else{
            return null;
        }

    }

    /**
     * Set actEntity
     *
     * @param string $actEntity
     *
     * @return Cfg_entity
     */
    public function setActEntity($actEntity)
    {
        $this->actEntity = $actEntity;

        return $this;
    }

    /**
     * Get actEntity
     *
     * @return string
     */
    public function getActEntity()
    {
        return $this->actEntity;
    }

    /**
     * Add entitySedeentity
     *
     * @param \cfg\userBundle\Entity\Cfg_sedeentity $entitySedeentity
     *
     * @return Cfg_entity
     */
    public function addEntitySedeentity(\cfg\userBundle\Entity\Cfg_sedeentity $entitySedeentity)
    {
        $this->entitySedeentity[] = $entitySedeentity;

        return $this;
    }

    /**
     * Remove entitySedeentity
     *
     * @param \cfg\userBundle\Entity\Cfg_sedeentity $entitySedeentity
     */
    public function removeEntitySedeentity(\cfg\userBundle\Entity\Cfg_sedeentity $entitySedeentity)
    {
        $this->entitySedeentity->removeElement($entitySedeentity);
    }

    /**
     * Get entitySedeentity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntitySedeentity()
    {
        return $this->entitySedeentity;
    }

    /**
     * Add entityUser
     *
     * @param \cfg\userBundle\Entity\Cfg_user $entityUser
     *
     * @return Cfg_entity
     */
    public function addEntityUser(\cfg\userBundle\Entity\Cfg_user $entityUser)
    {
        $this->entityUser[] = $entityUser;

        return $this;
    }

    /**
     * Remove entityUser
     *
     * @param \cfg\userBundle\Entity\Cfg_user $entityUser
     */
    public function removeEntityUser(\cfg\userBundle\Entity\Cfg_user $entityUser)
    {
        $this->entityUser->removeElement($entityUser);
    }

    /**
     * Get entityUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntityUser()
    {
        return $this->entityUser;
    }

    /**
     * Set sedeEntity
     *
     * @param string $sedeEntity
     *
     * @return Cfg_entity
     */
    public function setSedeEntity($sedeEntity)
    {
        $this->sedeEntity = $sedeEntity;

        return $this;
    }

    /**
     * Get sedeEntity
     *
     * @return string
     */
    public function getSedeEntity()
    {
        return $this->sedeEntity;
    }

    /**
     * Set identEntity
     *
     * @param string $identEntity
     *
     * @return Cfg_entity
     */
    public function setIdentEntity($identEntity)
    {
        $this->identEntity = $identEntity;

        return $this;
    }

    /**
     * Get identEntity
     *
     * @return string
     */
    public function getIdentEntity()
    {
        return $this->identEntity;
    }
}
