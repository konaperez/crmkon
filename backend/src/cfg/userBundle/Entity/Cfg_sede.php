<?php

namespace cfg\userBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Cfg_sede
 *
 * @ORM\Table(name="cfg_sede")
 * @ORM\Entity(repositoryClass="cfg\userBundle\Repository\Cfg_sedeRepository")
 */
class Cfg_sede
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 255 caracteres"
     * )
     * @ORM\Column(name="name_sede", type="string", length=255, unique=true)
     */
    private $nameSede;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 255 caracteres"
     * )
     * @ORM\Column(name="city_sede", type="string", length=255)
     */
    private $citySede;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Este dato debe ser mayor a 3 carácter",
     *      maxMessage = "Este dato debe ser menor a 255 caracteres"
     * )
     * @ORM\Column(name="state_sede", type="string", length=255)
     */
    private $stateSede;

    /**
     * @var string
     *
     * @ORM\Column(name="address_sede", type="string", length=255, nullable=true)
     */
    private $addressSede;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_sede", type="string", length=255, nullable=true)
     */
    private $contactSede;

    /**
     * @var string
     *
     * @ORM\Column(name="email_sede", type="string", length=255, nullable=true)
     */
    private $emailSede;

    /**
     * @var string
     *
     * @ORM\Column(name="web_sede", type="string", length=255, nullable=true)
     */
    private $webSede;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="act_sede", type="string", length=2)
     */
    private $actSede;

    /**
     * @var string
     *
     * @ORM\Column(name="obs_sede", type="text", nullable=true)
     */
    private $obsSede;




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sedeSedeentity = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameSede
     *
     * @param string $nameSede
     *
     * @return Cfg_sede
     */
    public function setNameSede($nameSede)
    {
        $this->nameSede = $nameSede;

        return $this;
    }

    /**
     * Get nameSede
     *
     * @return string
     */
    public function getNameSede()
    {
        return $this->nameSede;
    }

    /**
     * Set citySede
     *
     * @param string $citySede
     *
     * @return Cfg_sede
     */
    public function setCitySede($citySede)
    {
        $this->citySede = $citySede;

        return $this;
    }

    /**
     * Get citySede
     *
     * @return string
     */
    public function getCitySede()
    {
        return $this->citySede;
    }

    /**
     * Set stateSede
     *
     * @param string $stateSede
     *
     * @return Cfg_sede
     */
    public function setStateSede($stateSede)
    {
        $this->stateSede = $stateSede;

        return $this;
    }

    /**
     * Get stateSede
     *
     * @return string
     */
    public function getStateSede()
    {
        return $this->stateSede;
    }

    /**
     * Set addressSede
     *
     * @param string $addressSede
     *
     * @return Cfg_sede
     */
    public function setAddressSede($addressSede)
    {
        $this->addressSede = $addressSede;

        return $this;
    }

    /**
     * Get addressSede
     *
     * @return string
     */
    public function getAddressSede()
    {
        return $this->addressSede;
    }

    /**
     * Set contactSede
     *
     * @param string $contactSede
     *
     * @return Cfg_sede
     */
    public function setContactSede($contactSede)
    {
        $this->contactSede = $contactSede;

        return $this;
    }

    /**
     * Get contactSede
     *
     * @return string
     */
    public function getContactSede()
    {
        return $this->contactSede;
    }

    /**
     * Set emailSede
     *
     * @param string $emailSede
     *
     * @return Cfg_sede
     */
    public function setEmailSede($emailSede)
    {
        $this->emailSede = $emailSede;

        return $this;
    }

    /**
     * Get emailSede
     *
     * @return string
     */
    public function getEmailSede()
    {
        return $this->emailSede;
    }

    /**
     * Set webSede
     *
     * @param string $webSede
     *
     * @return Cfg_sede
     */
    public function setWebSede($webSede)
    {
        $this->webSede = $webSede;

        return $this;
    }

    /**
     * Get webSede
     *
     * @return string
     */
    public function getWebSede()
    {
        return $this->webSede;
    }

    /**
     * Set actSede
     *
     * @param string $actSede
     *
     * @return Cfg_sede
     */
    public function setActSede($actSede)
    {
        $this->actSede = $actSede;

        return $this;
    }

    /**
     * Get actSede
     *
     * @return string
     */
    public function getActSede()
    {
        return $this->actSede;
    }

    /**
     * Set obsSede
     *
     * @param string $obsSede
     *
     * @return Cfg_sede
     */
    public function setObsSede($obsSede)
    {
        $this->obsSede = $obsSede;

        return $this;
    }

    /**
     * Get obsSede
     *
     * @return string
     */
    public function getObsSede()
    {
        return $this->obsSede;
    }

    /**
     * Add sedeSedeentity
     *
     * @param \cfg\userBundle\Entity\Cfg_sedeentity $sedeSedeentity
     *
     * @return Cfg_sede
     */
    public function addSedeSedeentity(\cfg\userBundle\Entity\Cfg_sedeentity $sedeSedeentity)
    {
        $this->sedeSedeentity[] = $sedeSedeentity;

        return $this;
    }

    /**
     * Remove sedeSedeentity
     *
     * @param \cfg\userBundle\Entity\Cfg_sedeentity $sedeSedeentity
     */
    public function removeSedeSedeentity(\cfg\userBundle\Entity\Cfg_sedeentity $sedeSedeentity)
    {
        $this->sedeSedeentity->removeElement($sedeSedeentity);
    }

    /**
     * Get sedeSedeentity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSedeSedeentity()
    {
        return $this->sedeSedeentity;
    }
}
