<?php

namespace cfg\userBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cfg_permissionesp
 *
 * @ORM\Table(name="cfg_permissionesp")
 * @ORM\Entity(repositoryClass="cfg\userBundle\Repository\Cfg_permissionespRepository")
 */
class Cfg_permissionesp
{
    /**
     * @ORM\ManyToOne(targetEntity="cfg\userBundle\Entity\Cfg_user")
     * @ORM\JoinColumn(name="id_user_permissionesp", referencedColumnName="id",nullable=true,unique=true)
     */
    private $idUserPermissionesp;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="normalizeinstrument_permissionesp", type="string", length=2)
     */
    private $normalizeinstrumentPermissionesp;

    /**
     * @var string
     *
     * @ORM\Column(name="restartauto_permissionesp", type="string", length=2)
     */
    private $restartautoPermissionesp;

    /**
     * @var string
     *
     * @ORM\Column(name="copyauto_permissionesp", type="string", length=2)
     */
    private $copyautoPermissionesp;

    /**
     * @var string
     *
     * @ORM\Column(name="copypregunta_permissionesp", type="string", length=2)
     */
    private $copypreguntaPermissionesp;

    /**
     * @var string
     *
     * @ORM\Column(name="dbactor_permissionesp", type="string", length=2)
     */
    private $dbactorPermissionesp;

    /**
     * @var string
     *
     * @ORM\Column(name="activeinstrument_permissionesp", type="string", length=2)
     */
    private $activeinstrumentPermissionesp;

    /**
     * @var string
     *
     * @ORM\Column(name="accesoauto_permissionesp", type="string", length=50, nullable=true)
     */
    private $accesoautoPermissionesp;    



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set normalizeinstrumentPermissionesp
     *
     * @param string $normalizeinstrumentPermissionesp
     *
     * @return Cfg_permissionesp
     */
    public function setNormalizeinstrumentPermissionesp($normalizeinstrumentPermissionesp)
    {
        $this->normalizeinstrumentPermissionesp = $normalizeinstrumentPermissionesp;

        return $this;
    }

    /**
     * Get normalizeinstrumentPermissionesp
     *
     * @return string
     */
    public function getNormalizeinstrumentPermissionesp()
    {
        return $this->normalizeinstrumentPermissionesp;
    }

    /**
     * Set restartautoPermissionesp
     *
     * @param string $restartautoPermissionesp
     *
     * @return Cfg_permissionesp
     */
    public function setRestartautoPermissionesp($restartautoPermissionesp)
    {
        $this->restartautoPermissionesp = $restartautoPermissionesp;

        return $this;
    }

    /**
     * Get restartautoPermissionesp
     *
     * @return string
     */
    public function getRestartautoPermissionesp()
    {
        return $this->restartautoPermissionesp;
    }

    /**
     * Set copyautoPermissionesp
     *
     * @param string $copyautoPermissionesp
     *
     * @return Cfg_permissionesp
     */
    public function setCopyautoPermissionesp($copyautoPermissionesp)
    {
        $this->copyautoPermissionesp = $copyautoPermissionesp;

        return $this;
    }

    /**
     * Get copyautoPermissionesp
     *
     * @return string
     */
    public function getCopyautoPermissionesp()
    {
        return $this->copyautoPermissionesp;
    }

    /**
     * Set copypreguntaPermissionesp
     *
     * @param string $copypreguntaPermissionesp
     *
     * @return Cfg_permissionesp
     */
    public function setCopypreguntaPermissionesp($copypreguntaPermissionesp)
    {
        $this->copypreguntaPermissionesp = $copypreguntaPermissionesp;

        return $this;
    }

    /**
     * Get copypreguntaPermissionesp
     *
     * @return string
     */
    public function getCopypreguntaPermissionesp()
    {
        return $this->copypreguntaPermissionesp;
    }

    /**
     * Set dbactorPermissionesp
     *
     * @param string $dbactorPermissionesp
     *
     * @return Cfg_permissionesp
     */
    public function setDbactorPermissionesp($dbactorPermissionesp)
    {
        $this->dbactorPermissionesp = $dbactorPermissionesp;

        return $this;
    }

    /**
     * Get dbactorPermissionesp
     *
     * @return string
     */
    public function getDbactorPermissionesp()
    {
        return $this->dbactorPermissionesp;
    }

    /**
     * Set activeinstrumentPermissionesp
     *
     * @param string $activeinstrumentPermissionesp
     *
     * @return Cfg_permissionesp
     */
    public function setActiveinstrumentPermissionesp($activeinstrumentPermissionesp)
    {
        $this->activeinstrumentPermissionesp = $activeinstrumentPermissionesp;

        return $this;
    }

    /**
     * Get activeinstrumentPermissionesp
     *
     * @return string
     */
    public function getActiveinstrumentPermissionesp()
    {
        return $this->activeinstrumentPermissionesp;
    }

    /**
     * Set idUserPermissionesp
     *
     * @param \cfg\userBundle\Entity\Cfg_user $idUserPermissionesp
     *
     * @return Cfg_permissionesp
     */
    public function setIdUserPermissionesp(\cfg\userBundle\Entity\Cfg_user $idUserPermissionesp = null)
    {
        $this->idUserPermissionesp = $idUserPermissionesp;

        return $this;
    }

    /**
     * Get idUserPermissionesp
     *
     * @return \cfg\userBundle\Entity\Cfg_user
     */
    public function getIdUserPermissionesp()
    {
        return $this->idUserPermissionesp;
    }

    /**
     * Set accesoautoPermissionesp
     *
     * @param string $accesoautoPermissionesp
     *
     * @return Cfg_permissionesp
     */
    public function setAccesoautoPermissionesp($accesoautoPermissionesp)
    {
        $this->accesoautoPermissionesp = $accesoautoPermissionesp;

        return $this;
    }

    /**
     * Get accesoautoPermissionesp
     *
     * @return string
     */
    public function getAccesoautoPermissionesp()
    {
        return $this->accesoautoPermissionesp;
    }
}
