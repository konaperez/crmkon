<?php

namespace cfg\userBundle\Controller;

use cfg\userBundle\Entity\Cfg_permission;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * Cfg_permission controller.
 *
 */
class Cfg_permissionController extends Controller
{
    /**
     * Lists all cfg_permission entities.
     *
     */
    public function indexAction()
    {
        //***********************segutity
        if($this->get('funciones')->validsessionuser('user') == 0 ){
            return $this->render('view_errors/error_session.html.twig');
        } 
        //***********************segutity        
        $em = $this->getDoctrine()->getManager();

        $cfg_role = $em->createQuery(" SELECT m
                FROM cfguserBundle:Cfg_role m    
                WHERE m.nameRole != 'ROOT' ORDER BY m.id ASC")->getResult();

        $cfg_permissions = $em->getRepository('cfguserBundle:Cfg_permission')->findAll();
               
        return $this->render('cfg_permission/index.html.twig', array(
            'cfg_permissions' => $cfg_permissions,
            'cfg_role' => $cfg_role 
        ));
    }

    /**
     * Creates a new cfg_permission entity.
     *
     */
    public function newAction(Request $request,$id)
    {

        //***********************segutity        
        $em = $this->getDoctrine()->getManager();
        $jwt =  $this->get('helpers');
        $json = json_decode($_POST['json']);        
        $existe = $em->getRepository('cfguserBundle:Cfg_permission')
        ->findOneBy(array('idRolePermission'=>$id,'idUrlPermission'=>$json->url));
        if(is_object($existe)){
            if($json->type=='new_permission'){
                $existe->setNewPermission($json->estado);
                }
                if($json->type=='delete_permission'){
                $existe->setDeletePermission($json->estado);
                }
                if($json->type=='list_permission'){
                $existe->setListPermission($json->estado);
                }
                if($json->type=='edit_permission'){
                $existe->setEditPermission($json->estado);
                }
                if($json->type=='show_permission'){
                $existe->setShowPermission($json->estado);
                }
                if($json->type=='cause_permission'){
                $existe->setCausePermission($json->estado);
                }
                if($json->type=='descause_permission'){
                $existe->setDescausePermission($json->estado);
                }
                if($json->type=='visible_permission'){
                $existe->setVisiblePermission($json->estado);
                }   
                $em->persist($existe);
                $em->flush();
        }else{
            $obj = new Cfg_permission;
            $obj->setIdUrlPermission($em->getRepository('cfguserBundle:Cfg_url')->find($json->url));            
            $obj->setIdRolePermission($em->getRepository('cfguserBundle:Cfg_role')->find($id));

                if($json->type=='new_permission'){
                $obj->setNewPermission($json->estado);
                }
                if($json->type=='delete_permission'){
                $obj->setDeletePermission($json->estado);
                }
                if($json->type=='list_permission'){
                $obj->setListPermission($json->estado);
                }
                if($json->type=='edit_permission'){
                $obj->setEditPermission($json->estado);
                }
                if($json->type=='show_permission'){
                $obj->setShowPermission($json->estado);
                }
                if($json->type=='cause_permission'){
                $obj->setCausePermission($json->estado);
                }
                if($json->type=='descause_permission'){
                $obj->setDescausePermission($json->estado);
                }
                if($json->type=='visible_permission'){
                $obj->setVisiblePermission($json->estado);
                }   
                $em->persist($obj);
                $em->flush();
        }

        return new JsonResponse(['success'=>'OK','data'=> $json->url]);
        
/*        
        $session = new Session();
        $user = $_POST['role'];
        $em->getRepository('cfguserBundle:Cfg_permission')->findAll();
        $cfg_user = $em->createQuery(" DELETE
        FROM cfguserBundle:Cfg_permission m  WHERE m.idRolePermission =$user")->execute();
 
        $cfg_url=$em->getRepository('cfguserBundle:Cfg_url')->findAll();

        foreach ($cfg_url as $value) {
           $cfg_permission = new Cfg_permission();
           $cfg_permission->setListPermission($_POST['list'.$value->getId()]);
           $cfg_permission->setShowPermission($_POST['show'.$value->getId()]);
           $cfg_permission->setEditPermission($_POST['edit'.$value->getId()]);
           $cfg_permission->setNewPermission($_POST['new'.$value->getId()]);
           $cfg_permission->setDeletePermission($_POST['delete'.$value->getId()]);
           $cfg_permission->setCausePermission($_POST['cause'.$value->getId()]);
           $cfg_permission->setDescausePermission($_POST['descause'.$value->getId()]);
           $cfg_permission->setVisiblePermission($_POST['visible'.$value->getId()]);
           $cfg_permission->setIdUrlPermission($em->getRepository('cfguserBundle:Cfg_url')->find($value->getId()));
           $cfg_permission->setIdRolePermission($em->getRepository('cfguserBundle:Cfg_role')->find($_POST['role']));
           $em->persist($cfg_permission);
           $em->flush();           
        }

                                return new Response(json_encode(array(
                                'res'    => true,
                                'errors' => ''
                                    ))); 
*/




    }

    /**
     * Finds and displays a cfg_permission entity.
     *
     */
    public function showAction($id)
    {
        //***********************segutity
        if($this->get('funciones')->validsessionuser('user') == 0 ){
            return $this->render('view_errors/error_session.html.twig');
        } 
        //***********************segutity        
        $em = $this->getDoctrine()->getManager();
        $cfg_url=$em->getRepository('cfguserBundle:Cfg_url')->findAll();

        return $this->render('cfg_permission/show.html.twig', array(
            'cfg_url'=>$cfg_url,
            'rol'=>$id
        ));
    }

    /**
     * Displays a form to edit an existing cfg_permission entity.
     *
     */
    public function editAction(Request $request, Cfg_permission $cfg_permission)
    {
        //***********************segutity
        if($this->get('funciones')->validsessionuser('user') == 0 ){
            return $this->render('view_errors/error_session.html.twig');
        } 
        //***********************segutity        
        $deleteForm = $this->createDeleteForm($cfg_permission);
        $editForm = $this->createForm('cfg\userBundle\Form\Cfg_permissionType', $cfg_permission);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cfg_permission_edit', array('id' => $cfg_permission->getId()));
        }

        return $this->render('cfg_permission/edit.html.twig', array(
            'cfg_permission' => $cfg_permission,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cfg_permission entity.
     *
     */
    public function deleteAction(Request $request, Cfg_permission $cfg_permission)
    {
        //***********************segutity
        if($this->get('funciones')->validsessionuser('user') == 0 ){
            return $this->render('view_errors/error_session.html.twig');
        } 
        //***********************segutity        
        $form = $this->createDeleteForm($cfg_permission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cfg_permission);
            $em->flush();
        }

        return $this->redirectToRoute('cfg_permission_index');
    }

    /**
     * Creates a form to delete a cfg_permission entity.
     *
     * @param Cfg_permission $cfg_permission The cfg_permission entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cfg_permission $cfg_permission)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cfg_permission_delete', array('id' => $cfg_permission->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function listpermissionAction(Request $request,$id){
        $em = $this->getDoctrine()->getManager();
        
        $arr=array();

        foreach($em->getRepository('cfguserBundle:Cfg_url')->findAll() as $key){
            $per=$em->getRepository('cfguserBundle:Cfg_permission')
            ->findOneBy(array('idUrlPermission'=>$key->getId(),'idRolePermission'=>$id));
            if(is_object($per)){
                array_push($arr,array(
                    'new_permission'=>$per->getNewPermission(),
                    'delete_permission'=>$per->getDeletePermission(),
                    'list_permission'=>$per->getListPermission(),
                    'edit_permission'=>$per->getEditPermission(),
                    'show_permission'=>$per->getShowpermission(),
                    'cause_permission'=>$per->getCausePermission(),
                    'descause_permission'=>$per->getDescausePermission(),
                    'visible_permission'=>$per->getVisiblePermission(),
                    'esmodule_url'=>$key->getEsmoduleUrl(),
                    'obj_url'=>$key->getObjUrl(),
                    'nameaccess_url'=>$key->getNameaccessUrl(),
                    'name_url'=>$key->getNameUrl(),
                    'id'=>$key->getId()
                    ));
            }else{
                array_push($arr,array('new_permission'=>'NO',
                'delete_permission'=>'NO',
                'list_permission'=>'NO',
                'edit_permission'=>'NO',
                'show_permission'=>'NO',
                'cause_permission'=>'NO',
                'descause_permission'=>'NO',
                'visible_permission'=>'NO',
                'esmodule_url'=>$key->getEsmoduleUrl(),
                'obj_url'=>$key->getObjUrl(),
                'nameaccess_url'=>$key->getNameAccessUrl(),
                'name_url'=>$key->getNameUrl(),
                'id'=>$key->getId()));                
            }           
        }
        return new JsonResponse(['data'=> $arr]);
        
       
    }



}
