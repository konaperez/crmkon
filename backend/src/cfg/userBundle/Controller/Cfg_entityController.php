<?php

namespace cfg\userBundle\Controller;

use cfg\userBundle\Entity\Cfg_entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Cfg_entity controller.
 *
 */
class Cfg_entityController extends Controller
{
    public function rutaproyecto(){
            return  preg_replace('/app..../i', '', $this->container->getParameter('kernel.root_dir') . '/../');
    } 

    /**
     * Lists all cfg_entity entities.
     *
     */
    public function indexAction()
    {
    }

    /**
     * Creates a new cfg_entity entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();    
        $json =json_decode($_POST['json']);

            
        if($json->id != 0){
            $obj = $em->getRepository('cfguserBundle:Cfg_entity')->find($json->id);
        }else{
            $obj = new Cfg_entity();
        }
        $obj->setNameEntity($json->nameEntity);
        $obj->setAddressEntity($json->addressEntity);
        $obj->setPhoneEntity($json->phoneEntity);
        $obj->setEmailEntity($json->emailEntity);
        $obj->setObsEntity($json->obsEntity);
        $obj->setWebEntity($json->webEntity);
        $obj->setCreateonEntity(new \DateTime());
        $obj->setUpdateonEntity(new \DateTime());
        $obj->setCityEntity($json->cityEntity);
        $obj->setDepartamentoEntity($json->departamentoEntity);
        $obj->setImgEntity($json->imgEntity);
        $obj->setActEntity($json->actEntity);
        $obj->setSedeEntity($json->sedeEntity);
        $obj->setIdentEntity($json->identEntity);



        $validator = $this->get('validator');
        $errors = $validator->validate($obj);
        $array = array();
        if (count($errors) > 0) {
             foreach ($errors as $error) {
                 array_push($array, array($error->getPropertyPath() => $error->getMessage()));
             }
         }
 
         if(count($errors)==0){
 
             $em->persist($obj);
             $em->flush();
             return new JsonResponse(['success'=>'OK']);
             
         }
        return new JsonResponse(['permisos'=>'OK','errores'=>$array]);        
    }

    /**
     * Finds and displays a cfg_entity entity.
     *
     */
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();  
        $datos_mostrar=5;              
        $json =json_decode($_POST['json']);  
        
        $sql = $em->getRepository("cfguserBundle:Cfg_entity");
        $sql = $sql->createQueryBuilder('m');
        $sql = $sql->select(array('m.id','m.nameEntity','m.emailEntity','m.phoneEntity'));

        if($json->name != "" ){
                $sql->where("m.nameEntity LIKE '".$json->name."%'");
                $sql_count = $em->createQuery(" SELECT count(m) as cont
                FROM cfguserBundle:Cfg_entity m WHERE  m.nameEntity LIKE '".$json->name."%'")
                ->getArrayResult();                   
        }else{
            $sql_count = $em->createQuery(" SELECT count(m) as cont
            FROM cfguserBundle:Cfg_entity m")->getArrayResult();
        }
                           
        $sql->orderBy("m.id", 'ASC');
        $sql->setFirstResult( $datos_mostrar * ( $json->page - 1)  );
        $sql->setMaxResults( $datos_mostrar );                       
        $sql=$sql->getQuery()->getArrayResult();        

        return new JsonResponse([
        'data'=>$sql,
        'total'=>$sql_count[0]['cont'],
        ]);        
    }

    /**
     * Displays a form to edit an existing cfg_entity entity.
     *
     */
    public function editAction(Request $request, Cfg_entity $cfg_entity)
    {
        $em = $this->getDoctrine()->getManager();        
        $json =json_decode($_POST['json']);
        $id=$cfg_entity->getId();
        $data = $em->createQuery(" SELECT m
                FROM cfguserBundle:Cfg_entity m 
                WHERE m.id =$id")->getArrayResult();        
        return new JsonResponse(['success'=>'OK','data'=>$data[0]]);        
    }

    /**
     * Deletes a cfg_entity entity.
     *
     */
    public function deleteAction(Request $request, Cfg_entity $cfg_entity)
    {
        $em = $this->getDoctrine()->getManager();        
        $em->getConnection()->beginTransaction();                 
      try{

        $em->remove($cfg_entity);
        $em->flush();

                    if($cfg_entity->getImgEntity() != null){
                        unlink($this->rutaproyecto().'web/'.$cfg_user->getImgEntity());                            
                    }

                $em->getConnection()->commit();  
                   return new JsonResponse(['success'=>'OK']);
                                  
       }catch(\Exception $ex){
                    $em->getConnection()->rollback();
                    return new JsonResponse(['success'=>'ERROR']);
                  
               }        
    }

    /**
     * Creates a form to delete a cfg_entity entity.
     *
     * @param Cfg_entity $cfg_entity The cfg_entity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cfg_entity $cfg_entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cfg_entity_delete', array('id' => $cfg_entity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

protected function getErrorsAsArray($form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error)
            $errors[] = $error->getMessage();
 
        foreach ($form->all() as $key => $child) {
            if ($err = $this->getErrorsAsArray($child))
                $errors[$key] = $err;
        }
        return $errors;
    }  

}
