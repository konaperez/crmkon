<?php

namespace cfg\userBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        //***********************segutity
        if($this->get('funciones')->validsessionuser('user') == 0 ){
            return $this->render('view_errors/error_session.html.twig');
        } 
        //***********************segutity    	
        return $this->render('cfguserBundle:Default:index.html.twig');
    }
}
