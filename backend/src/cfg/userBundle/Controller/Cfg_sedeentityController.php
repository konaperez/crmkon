<?php

namespace cfg\userBundle\Controller;

use cfg\userBundle\Entity\Cfg_sedeentity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * Cfg_sedeentity controller.
 *
 */
class Cfg_sedeentityController extends Controller
{
    /**
     * Lists all cfg_sedeentity entities.
     *
     */
    public function indexAction()
    {

    }

    /**
     * Creates a new cfg_sedeentity entity.
     *
     */
    public function newAction(Request $request)
    {
  
    }

    /**
     * Finds and displays a cfg_sedeentity entity.
     *
     */
    public function showAction(Cfg_sedeentity $cfg_sedeentity)
    {
    }

    /**
     * Displays a form to edit an existing cfg_sedeentity entity.
     *
     */
    public function editAction(Request $request, Cfg_sedeentity $cfg_sedeentity)
    {
    }

    /**
     * Deletes a cfg_sedeentity entity.
     *
     */
    public function deleteAction(Request $request, Cfg_sedeentity $cfg_sedeentity)
    {
    }

    /**
     * Creates a form to delete a cfg_sedeentity entity.
     *
     * @param Cfg_sedeentity $cfg_sedeentity The cfg_sedeentity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cfg_sedeentity $cfg_sedeentity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cfg_sedeentity_delete', array('id' => $cfg_sedeentity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
