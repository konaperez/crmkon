<?php

namespace cfg\userBundle\Controller;

use cfg\userBundle\Entity\Cfg_role;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * Cfg_role controller.
 *
 */
class Cfg_roleController extends Controller
{
    /**
     * Lists all cfg_role entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();  
        $datos_mostrar=5;              
        //$json =json_decode($_POST['json']);                
        $cfg_roles = $em->createQuery(" SELECT m
                FROM cfguserBundle:Cfg_role m    
                WHERE m.nameRole != 'ROOT' ORDER BY m.id ASC")->getArrayResult();

        return new JsonResponse(['data'=> $cfg_roles,'cont'=>$_POST]);
    }

    /**
     * Creates a new cfg_role entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();    
        $json =json_decode($_POST['json']);

            
        if($json->id != 0){
            $obj = $em->getRepository('cfguserBundle:Cfg_role')->find($json->id);
        }else{
            $obj = new Cfg_role();
        }

        $obj->setNameRole($json->nameRole);
        $obj->setActRole($json->actRole);

        $validator = $this->get('validator');
        $errors = $validator->validate($obj);
        $array = array();
        if (count($errors) > 0) {
             foreach ($errors as $error) {
                 array_push($array, array($error->getPropertyPath() => $error->getMessage()));
             }
         }
 
         if(count($errors)==0){
 
             $em->persist($obj);
             $em->flush();
             return new JsonResponse(['success'=>'OK']);
             
         }
        return new JsonResponse(['permisos'=>'OK','errores'=>$array]);
 
    }

    /**
     * Finds and displays a cfg_role entity.
     *
     */
    public function showAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();  
        $datos_mostrar=5;              
        $json =json_decode($_POST['json']);  
        
        $sql = $em->getRepository("cfguserBundle:Cfg_role");
        $sql = $sql->createQueryBuilder('m');
        if($json->name != "" ){
                $sql->where("m.nameRole LIKE '".$json->name."%'");
                $sql->andWhere("m.nameRole != 'ROOT'");
                $sql_count = $em->createQuery(" SELECT count(m) as cont
                FROM cfguserBundle:Cfg_role m WHERE  m.nameRole LIKE '".$json->name."%'")
                ->getArrayResult();                   
        }else{
            $sql->where("m.nameRole != 'ROOT'");
            $sql_count = $em->createQuery(" SELECT count(m) as cont
            FROM cfguserBundle:Cfg_role m WHERE m.nameRole != 'ROOT'")->getArrayResult();
        }
                           
        $sql->orderBy("m.id", 'ASC');
        $sql->setFirstResult( $datos_mostrar * ( $json->page - 1)  );
        $sql->setMaxResults( $datos_mostrar );                       
        $sql=$sql->getQuery()->getArrayResult();        

        return new JsonResponse([
        'data'=>$sql,
        'total'=>$sql_count[0]['cont'],
        ]);

    }
    /**
     * Displays a form to edit an existing cfg_role entity.
     *
     */
    public function editAction(Request $request, Cfg_role $cfg_role)
    {
        $em = $this->getDoctrine()->getManager();        
        $json =json_decode($_POST['json']);
        $id=$cfg_role->getId();
        $data = $em->createQuery(" SELECT m
                FROM cfguserBundle:Cfg_role m    
                WHERE m.id =$id")->getArrayResult();        
        return new JsonResponse(['success'=>'OK','data'=>$data[0]]);
    }

    /**
     * Deletes a cfg_role entity.
     *
     */
    public function deleteAction(Request $request, Cfg_role $cfg_role)
    {       
         $em = $this->getDoctrine()->getManager();        
         $em->getConnection()->beginTransaction();                 
       try{

            $em = $this->getDoctrine()->getManager();
            $em->remove($cfg_role);
            $em->flush();

                    $em->getConnection()->commit(); 
                    return new JsonResponse(['success'=>'OK']);
                                   
        }catch(\Exception $ex){
                     $em->getConnection()->rollback();
                     return new JsonResponse(['success'=>'ERROR']);
                   
                }
    }

    /**
     * Creates a form to delete a cfg_role entity.
     *
     * @param Cfg_role $cfg_role The cfg_role entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cfg_role $cfg_role)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cfg_role_delete', array('id' => $cfg_role->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

protected function getErrorsAsArray($form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error)
            $errors[] = $error->getMessage();
 
        foreach ($form->all() as $key => $child) {
            if ($err = $this->getErrorsAsArray($child))
                $errors[$key] = $err;
        }
        return $errors;
    }  

}
