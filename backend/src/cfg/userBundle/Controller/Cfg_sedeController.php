<?php

namespace cfg\userBundle\Controller;

use cfg\userBundle\Entity\Cfg_sede;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Cfg_sede controller.
 *
 */
class Cfg_sedeController extends Controller
{
    /**
     * Lists all cfg_sede entities.
     *
     */
    public function indexAction()
    {

    }

    /**
     * Creates a new cfg_sede entity.
     *
     */
    public function newAction(Request $request)
    {
    }

    /**
     * Finds and displays a cfg_sede entity.
     *
     */
    public function showAction(Cfg_sede $cfg_sede)
    {

    }

    /**
     * Displays a form to edit an existing cfg_sede entity.
     *
     */
    public function editAction(Request $request, Cfg_sede $cfg_sede)
    {
    }

    /**
     * Deletes a cfg_sede entity.
     *
     */
    public function deleteAction(Request $request, Cfg_sede $cfg_sede)
    {
    }

    /**
     * Creates a form to delete a cfg_sede entity.
     *
     * @param Cfg_sede $cfg_sede The cfg_sede entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cfg_sede $cfg_sede)
    {
        
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cfg_sede_delete', array('id' => $cfg_sede->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function jsonAction(){
    }  

}
