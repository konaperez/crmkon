<?php

namespace cfg\userBundle\Controller;

use cfg\userBundle\Entity\Cfg_user;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * Cfg_user controller.
 *
 */
class Cfg_userController extends Controller
{

    public function rutaproyecto(){
            return  preg_replace('/app..../i', '', $this->container->getParameter('kernel.root_dir') . '/../');
    }  

    /**
     * Lists all cfg_user entities.
     *
     */
    public function indexAction()
    {
 
    }

    /**
     * Creates a new cfg_user entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();    
        $json =json_decode($_POST['json']);

            
        if($json->id != 0){
            $obj = $em->getRepository('cfguserBundle:Cfg_user')->find($json->id);
        }else{
            $obj = new Cfg_user();
        }
        $obj->setIdRoleUser($em->getRepository('cfguserBundle:Cfg_role')->find($json->idRoleUser));
        $obj->setIdEntityUser($em->getRepository('cfguserBundle:Cfg_entity')->find($json->idEntityUser));
        $obj->setIdTypedocUser($em->getRepository('cfgtrBundle:Cfg_typedoc')->find($json->idTypedocUser));
        $obj->setNumidUser($json->numidUser);
        $obj->setImgUser($json->imgUser);
        $obj->setNameUser($json->nameUser);
        $obj->setImgUser($json->imgUser);
        $obj->setPasswdUser(sha1(md5($json->passwdUser)));
        $obj->setEmailUser($json->emailUser);
        $obj->setCreateonUser(new \DateTime());
        $obj->setLastaccessUser(new \DateTime());

        $obj->setActUser($json->actUser);
        $obj->setTypeUser('SISTEMA');
        $obj->setNameapeUser($json->nameapeUser);



        $validator = $this->get('validator');
        $errors = $validator->validate($obj);
        $array = array();
        if (count($errors) > 0) {
             foreach ($errors as $error) {
                 array_push($array, array($error->getPropertyPath() => $error->getMessage()));
             }
         }
 
         if(count($errors)==0){
 
             $em->persist($obj);
             $em->flush();
             return new JsonResponse(['success'=>'OK']);
             
         }
        return new JsonResponse(['permisos'=>'OK','errores'=>$array]);
    }

    /**
     * Finds and displays a cfg_user entity.
     *JOIN m.idEntityUser e
     */
    public function showAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();  
        $datos_mostrar=5;              
        $json =json_decode($_POST['json']);  
        
        $sql = $em->getRepository("cfguserBundle:Cfg_user");
        $sql = $sql->createQueryBuilder('m');
        $sql = $sql->select(array('m.id','m.actUser','m.nameapeUser','m.lastaccessUser','m.nameUser'));

        if($json->name != "" ){
                $sql->where("m.nameUser LIKE '".$json->name."%'");
                $sql->andWhere("m.typeUser != 'ROOT'");
                $sql_count = $em->createQuery(" SELECT count(m) as cont
                FROM cfguserBundle:Cfg_user m WHERE  m.nameUser LIKE '".$json->name."%'")
                ->getArrayResult();                   
        }else{
            $sql->where("m.typeUser != 'ROOT'");
            $sql_count = $em->createQuery(" SELECT count(m) as cont
            FROM cfguserBundle:Cfg_user m WHERE m.typeUser != 'ROOT'")->getArrayResult();
        }
                           
        $sql->orderBy("m.id", 'ASC');
        $sql->setFirstResult( $datos_mostrar * ( $json->page - 1)  );
        $sql->setMaxResults( $datos_mostrar );                       
        $sql=$sql->getQuery()->getArrayResult();        

        return new JsonResponse([
        'data'=>$sql,
        'total'=>$sql_count[0]['cont'],
        ]);
    }

    /**
     * Displays a form to edit an existing cfg_user entity.
     *
     */
    public function editAction(Request $request, Cfg_user $cfg_user)
    {
        $em = $this->getDoctrine()->getManager();        
        $json =json_decode($_POST['json']);
        $id=$cfg_user->getId();
        $data = $em->createQuery(" SELECT m.nameUser,m.nameapeUser,m.imgUser,m.passwdUser,m.actUser,m.emailUser,m.id,
        r.id as idRoleUser,e.id as idEntityUser,(SELECT t.id FROM cfgtrBundle:Cfg_typedoc t WHERE t.id = m.idTypedocUser) as idTypedocUser,m.numidUser
                FROM cfguserBundle:Cfg_user m 
                JOIN m.idRoleUser r
                JOIN m.idEntityUser e
                WHERE m.id =$id")->getArrayResult();        
        return new JsonResponse(['success'=>'OK','data'=>$data[0]]);
    }

    public function filesaddAction(Request $request){
		$bandera=0;
        $nombre='';
        
        $file =$_FILES['uploads'];
        $dir = 'repository/users/';
        $image_upload='';
                
            $archivo = preg_replace('[\s+]', "", $file['name'][0]);
            $image_upload = $dir .rand(0,100000)."_".uniqid().$archivo;            
            if (move_uploaded_file($file['tmp_name'][0], $image_upload)) {
                $bandera=1;

            }
        if($bandera==1){
            return new JsonResponse(['success'=>'OK','image_upload'=>$image_upload]);

        }else{
            return new JsonResponse(['success'=>'ERROR','image_upload'=>$image_upload]);

        }
           

    }

    /**
     * Deletes a cfg_user entity.
     *
     */
    public function deleteAction(Request $request, Cfg_user $cfg_user)
    {
        $em = $this->getDoctrine()->getManager();        
        $em->getConnection()->beginTransaction();                 
      try{

        $em->remove($cfg_user);
        $em->flush();

                    if($cfg_user->getImgUser() != null){
                        unlink($this->rutaproyecto().'web/'.$cfg_user->getImgUser());                            
                    }

                $em->getConnection()->commit();  
                   return new JsonResponse(['success'=>'OK']);
                                  
       }catch(\Exception $ex){
                    $em->getConnection()->rollback();
                    return new JsonResponse(['success'=>'ERROR']);
                  
               }

    }

    /**
     * Creates a form to delete a cfg_user entity.
     *
     * @param Cfg_user $cfg_user The cfg_user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cfg_user $cfg_user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cfg_user_delete', array('id' => $cfg_user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function entidadesAction(){
        $em = $this->getDoctrine()->getManager();        
        $roles = $em->createQuery(" SELECT m
                FROM cfguserBundle:Cfg_role m    
                WHERE m.nameRole !='ROOT' ")->getArrayResult(); 

        $entity = $em->createQuery(" SELECT m
        FROM cfguserBundle:Cfg_entity m")->getArrayResult();
        
        $typedoc = $em->createQuery(" SELECT m
        FROM cfgtrBundle:Cfg_typedoc m")->getArrayResult();        
                
                
        return new JsonResponse([
            'data'=>array('roles'=>$roles,'entity'=>$entity,'typedoc'=>$typedoc),
            'success'=>'OK',
            ]);
    }

    /**
     * Displays a form to edit an existing cfg_user entity.
     *
     */
    public function editperfilAction(Request $request, Cfg_user $cfg_user)
    {
        $em = $this->getDoctrine()->getManager();        
        $json =json_decode($_POST['json']);
        $id=$cfg_user->getId();
        $data = $em->createQuery(" SELECT m.imgUser,m.passwdUser,m.id
                FROM cfguserBundle:Cfg_user m 
                WHERE m.id =$id")->getArrayResult();        
        return new JsonResponse(['success'=>'OK','data'=>$data[0]]);   
    }

    public function updateperfilAction(Request $request){
        $em = $this->getDoctrine()->getManager();    
        $json =json_decode($_POST['json']);

        $obj = $em->getRepository('cfguserBundle:Cfg_user')->find($json->id);            
        $obj->setImgUser($json->imgUser);
        $obj->setPasswdUser($json->passwdUser);
        $obj->setUpdateonUser(new \DateTime());

        $validator = $this->get('validator');
        $errors = $validator->validate($obj);
        $array = array();
        if (count($errors) > 0) {
             foreach ($errors as $error) {
                 array_push($array, array($error->getPropertyPath() => $error->getMessage()));
             }
         }
 
         if(count($errors)==0){
 
             $em->persist($obj);
             $em->flush();
             return new JsonResponse(['success'=>'OK']);
             
         }
        return new JsonResponse(['permisos'=>'ERROR','errores'=>$array]);        
    }

protected function getErrorsAsArray($form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error)
            $errors[] = $error->getMessage();
 
        foreach ($form->all() as $key => $child) {
            if ($err = $this->getErrorsAsArray($child))
                $errors[$key] = $err;
        }
        return $errors;
    }  

}
