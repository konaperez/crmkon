<?php

namespace cfg\userBundle\Controller;

use cfg\userBundle\Entity\Cfg_permissionesp;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * Cfg_permissionesp controller.
 *
 */
class Cfg_permissionespController extends Controller
{
    /**
     * Lists all cfg_permissionesp entities.
     *
     */
    public function indexAction()
    {
 
    }

    /**
     * Creates a new cfg_permissionesp entity.
     *
     */
    public function newAction(Request $request)
    {
    }

    /**
     * Finds and displays a cfg_permissionesp entity.
     *
     */
    public function showAction(Cfg_permissionesp $cfg_permissionesp)
    {
    }

    /**
     * Displays a form to edit an existing cfg_permissionesp entity.
     *
     */
    public function editAction(Request $request, Cfg_permissionesp $cfg_permissionesp)
    {
    }

    /**
     * Deletes a cfg_permissionesp entity.
     *
     */
    public function deleteAction(Request $request, Cfg_permissionesp $cfg_permissionesp)
    {
    }

    /**
     * Creates a form to delete a cfg_permissionesp entity.
     *
     * @param Cfg_permissionesp $cfg_permissionesp The cfg_permissionesp entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cfg_permissionesp $cfg_permissionesp)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cfg_permissionesp_delete', array('id' => $cfg_permissionesp->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
