<?php 

namespace AppBundle\Services;
use \Firebase\JWT\JWT;

class Helpers{

	public $manager;
    const SECRET_TOKEN = 'jwtcunixtoken';

	public function __construct($manager){
		$this->manager = $manager;

	}

	public function json($data){
		$normalizers = array(new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer());
		$encoders = array("json"=> new \Symfony\Component\Serializer\Encoder\JsonEncoder());

		$serializer = new \Symfony\Component\Serializer\Serializer($normalizers,$encoders);
		$json = $serializer->serialize($data,'json');

		$response = new \Symfony\Component\HttpFoundation\Response();
		$response->setContent($json);
		$response->headers->set('Content-Type','application/json');
		return $response;
	}

	public function servicio()
	{
		return "hola mundo";
	}

   public function signup($em,$user,$password,$gethash){
        //$em = $this->getDoctrine()->getManager();  
        try{        




        $query = $em->createQuery(
            'SELECT p
            FROM cfguserBundle:Cfg_user p
            WHERE p.nameUser = :nameUser 
            AND  p.passwdUser = :passwdUser
            AND p.actUser = :actUser
          ')->setParameter('nameUser',$user)
            ->setParameter('passwdUser',$password)
            ->setParameter('actUser','SI')
            ->getArrayResult();  
            
            if(sizeof($query)>0){

                $data_token=array(
                    'nameUser'=>$query[0]['nameUser'],
                    'passwordUser'=>$query[0]['passwdUser'],
                    'emailUser'=>$query[0]['emailUser'],
                    'idUser'=>$query[0]['id'],                                                
                    'typeUser'=>$query[0]['typeUser'],
                    'iat'=>time(),
                    'exp'=>time()+(7*24*60*60)                                                
                );

                

                    $jwt = JWT::encode($data_token,self::SECRET_TOKEN);
                    $decoded = JWT::decode($jwt, self::SECRET_TOKEN, array('HS256'));
                    $data=array(
                        'success'=>'OK',
                        'token'=>$jwt,
                        'data'=>$decoded
                    ); 
                    

            }else{
                $data=array(
                    'success'=>'ERROR'
                );
            }

        }catch(\Exception $e){
             $data = $e->getMessage(). "\n";
       }
       return $data;

    }


    public function verify($token){
        $data=null;

        try{ 
            $decoded = JWT::decode($token, self::SECRET_TOKEN, array('HS256'));
        }catch(\Exception $e){
                $data = $e->getMessage();
        }        
        if(isset($decoded)){
            return array('success'=>'OK','data'=>$decoded);
        }else{
            return array('success'=>'ERROR','data'=>$data);
        }

    }


}


?>
