<?php
namespace AppBundle\Utils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager; 
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Response;
class Utils 
{
public $em;

public function __construct($em)
{
    $this->em = $em;
}


     public function permisosAll()
    {
		$session = new Session();
		$array=array();


        if($session->get('user')['type']=='ROOT'){
                $modulos = $this->em->createQuery(" SELECT m.id as id, 
                    m.nameUrl as name,
                    m.nameaccessUrl as access,
                    m.esmoduleUrl as esmodule,
                    m.objUrl as obj,
                    m.pathUrl as url,
                    m.actUrl as acturl
                    FROM cfguserBundle:Cfg_url m ORDER BY m.id ASC")->getArrayResult();


                foreach ($modulos as  $key) {

                    array_push($array, array(
                        'id'=>$key['id'],
                        'name'=> $key['name'],
                        'access'=>$key['access'],
                        'esmodule'=>$key['esmodule'],
                        'obj'=>$key['obj'],
                        'url'=>$key['url'],
                        'acturl'=>$key['acturl'],
                        'show'=>'SI',
                        'delete'=>'SI',
                        'new'=>'SI',
                        'edit'=>'SI',
                        'list'=>'SI',
                        'cause'=>'SI',
                        'descause'=>'SI',
                        'visible'=>'SI'
                        ));
                } 
        }else{
            
            $idins = $session->get('user')['ins'];
            $iduser = $session->get('user')['id'];

            $grupos = 
            	$this->em->createQuery("
            	SELECT m FROM cfguserBundle:Cfg_grupouser m
            	JOIN m.idUserGrupouser u 
            	JOIN m.idGrupoGrupouser g 
            	JOIN g.idInsGrupo i 
            	WHERE u.id = $iduser AND i.id = $idins ")->getResult();

            $url = $this->em->getRepository('cfguserBundle:Cfg_url')->findAll();

            if($grupos){
            	foreach ($grupos as $g) {

		            	foreach ($url as $u) {
		            		
		            		$permiso = $this->em->getRepository('cfguserBundle:Cfg_permission')
		            		->findBy(array('idUrlPermission'=>$u->getId(),'idGrupoPermission'=>$g->getIdGrupoGrupouser()->getId()));

		            		$show='NO';$edit='NO';$new='NO';$list='NO';
		            		$delete='NO';$cause='NO';$descause='NO';$visible='NO';

		            		if($permiso){
		            			foreach ($permiso as $p) {
		            				if($p->getShowPermission()=='SI'){     $show = 'SI';  }
		            				if($p->getEditPermission()=='SI'){     $edit = 'SI';  }
		            				if($p->getNewPermission()=='SI') {     $new = 'SI';  }
		            				if($p->getListPermission()=='SI'){     $show = 'SI';  }
		            				if($p->getDeletePermission()=='SI'){   $delete = 'SI';  }
		            				if($p->getCausePermission()=='SI'){    $cause = 'SI';  }
		            				if( strcmp($p->getDescausePermission(),'SI') == 0 ){ $descause = 'SI';  }
		            				if( strcmp($p->getVisiblePermission(), 'SI') == 0){  $visible = 'SI';  }

		            			}

		            		}//end if permission


			                    array_push($array, array(
			                        'id'=>$u->getId(),
			                        'name'=> $u->getNameUrl(),
			                        'access'=>$u->getNameaccessUrl(),
			                        'esmodule'=>$u->getEsmoduleUrl(),
			                        'obj'=>$u->getObjUrl(),
			                        'url'=>$u->getPathUrl(),
                                    'acturl'=>$u->getActUrl(),
			                        'show'=>$show,
			                        'delete'=>$delete,
			                        'new'=>$new,
			                        'edit'=>$edit,
			                        'list'=>$list,
			                        'cause'=>$cause,
			                        'descause'=>$descause,
			                        'visible'=>$visible
			                        ));

		            	}//end url            		

            	} // end foreach grupos

            }//if grupo

        }
        return $array;        
    }




     public function permisosOne($objeto)
    {
        $session = new Session();
		$array=array();


        if($session->get('user')['type']=='ROOT'){
                $modulos = $this->em->createQuery(" SELECT m.id as id, 
                    m.nameUrl as name,
                    m.nameaccessUrl as access,
                    m.esmoduleUrl as esmodule,
                    m.objUrl as obj,
                    m.pathUrl as url,
                    m.actUrl as acturl
                    FROM cfguserBundle:Cfg_url m 
                    WHERE m.nameaccessUrl = '".$objeto."'
                    ORDER BY m.id ASC")->getArrayResult();


                foreach ($modulos as  $key) {

                    array_push($array, array(
                        'id'=>$key['id'],
                        'name'=> $key['name'],
                        'access'=>$key['access'],
                        'esmodule'=>$key['esmodule'],
                        'obj'=>$key['obj'],
                        'url'=>$key['url'],
                        'acturl'=>$key['acturl'],
                        'show'=>'SI',
                        'delete'=>'SI',
                        'new'=>'SI',
                        'edit'=>'SI',
                        'list'=>'SI',
                        'cause'=>'SI',
                        'descause'=>'SI',
                        'visible'=>'SI'
                        ));
                } 
        }else{


            $idins = $session->get('user')['ins'];
            $iduser = $session->get('user')['id'];

            $grupos = 
            	$this->em->createQuery("
            	SELECT m FROM cfguserBundle:Cfg_grupouser m
            	JOIN m.idUserGrupouser u 
            	JOIN m.idGrupoGrupouser g 
            	JOIN g.idInsGrupo i 
            	WHERE u.id = $iduser AND i.id = $idins ")->getResult();

            $url = $this->em->getRepository('cfguserBundle:Cfg_url')->findOneBy(array('pathUrl'=>$objeto));


            if($grupos){

            	foreach ($grupos as $g) {

		            		
		            		$permiso = $this->em->getRepository('cfguserBundle:Cfg_permission')
		            		->findBy(array('idUrlPermission'=>$url->getId(),
                                'idGrupoPermission'=>$g->getIdGrupoGrupouser()->getId()
                                ));

                            $show='NO';$edit='NO';$new='NO';$list='NO';
		            		$delete='NO';$cause='NO';$descause='NO';$visible='NO';
		            		if($permiso){
		            			foreach ($permiso as $p) {
		            				if($p->getShowPermission()=='SI'){     $show = 'SI';  }
		            				if($p->getEditPermission()=='SI'){     $edit = 'SI';  }
		            				if($p->getNewPermission()=='SI') {     $new = 'SI';  }
		            				if($p->getListPermission()=='SI'){     $show = 'SI';  }
		            				if($p->getDeletePermission()=='SI'){   $delete = 'SI';  }
		            				if($p->getCausePermission()=='SI'){    $cause = 'SI';  }
		            				if($p->getDescausePermission()=='SI'){ $descause = 'SI';  }
		            				if($p->getVisiblePermission()=='SI'){  $visible = 'SI';  }

		            			}
		            		}//end if permission

			                    array_push($array, array(
			                        'id'=>$url->getId(),
			                        'name'=> $url->getNameUrl(),
			                        'access'=>$url->getNameaccessUrl(),
			                        'esmodule'=>$url->getEsmoduleUrl(),
			                        'obj'=>$url->getObjUrl(),
			                        'url'=>$url->getPathUrl(),
                                    'acturl'=>$url->getActUrl(),
			                        'show'=>$show,
			                        'delete'=>$delete,
			                        'new'=>$new,
			                        'edit'=>$edit,
			                        'list'=>$list,
			                        'cause'=>$cause,
			                        'descause'=>$descause,
			                        'visible'=>$visible
			                        ));
         		

            	} // end foreach grupos

            }//if grupo






        }


        return $array;
    }    

public function validpermission($idGrupo,$url){
                return $this->em->createQuery(" SELECT m
                    FROM cfguserBundle:Cfg_permission m 
                    WHERE m.idGrupoPermission = ".$idGrupo." AND
                    m.idUrlPermission = ".$url)->getArrayResult();
}


    /*******************************validando session usuario***********************/
    public function validsessionuser(){
        $session = new Session();
        if(!$session->get('user')){
            return 0;

        }else{
            return 1;
        }

                      
    }

    /*******************************validaciones historia clinica***********************/
    /*******************************validaciones historia clinica***********************/
    /*******************************validaciones historia clinica***********************/
    public function validacionhco($entityanterior,$entitysiguiente,$validar){
        
        $session= new Session();
        $valid = $this->em->getRepository('admadmBundle:Adm_adm')->find($session->get('adm'));
        $empresa = $valid->getIdEmpresaAdm()->getId();
        $idPacienteAdm = $valid->getIdPacienteAdm()->getId();
        $numHco = $valid->getNumhcAdm();
        $idAdmision =  $valid->getId();
        $array=array();
        $adm = $this->em->getRepository('admadmBundle:Adm_adm')->find($session->get('adm'));

        if($entitysiguiente == 'Hco' || $entitysiguiente == 'Familiar' || $entitysiguiente == 'Personal'){

            /////////////////////////////////////////////////////

          
            /**************hco*/
                $val = $this->em->getRepository('admhcBundle:Adm_'.strtolower($entityanterior))
                ->findOneBy(array('idAdm'.$entityanterior=>$idAdmision,'numHco'=>$numHco,'status'.$entityanterior=>1));
                    $array['message']='Por favor ingrese informacion de el anterior item para acceder a esta sección';

                if($val){
                    
                    $datos = $this->em->getRepository('admhcBundle:Adm_'.strtolower($entitysiguiente))
                    ->findOneBy(array('numHco'=>$numHco,'status'.$entitysiguiente=>1));

                    if($adm->getTypehcoAdm()=='RETIRO' || $adm->getTypehcoAdm()=='PERIODICA'){
                        $array['type']='RETIRO_PERIODICA';
                        if($datos){
                            $array['id']=$datos->getId();
                            
                            if($valid->getEstadoAdm()== 'ACTIVO'){
                                $array['activo']=1;                                
                            }else{
                                $array['activo']=0;
                            }
                            $array['status']=1;
                            $array['existedatos']=1; 
                        }else{
                            $array['status']=0;
                            $array['existedatos']=0;                         
                            $array['message']='No existen datos para esta sección';
                        }
                    }else{
                        $array['type']='OTRO';
                        if($datos){
                            $array['existedatos']=1; 
                                $array['id']=$datos->getId();                                                                                
                            if($valid->getEstadoAdm()== 'ACTIVO'){
                                $array['status']=1;  
                                $array['activo']=1;                                                                
                            }else{
                                $array['status']=1;
                                $array['activo']=0;                                                                
                            }                            
                        }else{
                            $array['existedatos']=0;                                                     
                            if($valid->getEstadoAdm()== 'ACTIVO'){
                                $array['status']=1;  
                                $array['activo']=1;                                                              
                            }else{
                                $array['status']=0;
                                $array['message']='No existen datos para esta sección';
                                $array['activo']=0;                                
                            } 
                        }                        

                    }
                  
                }else{
                  $array['status']=0;  
                  $array['message']='Por favor ingrese inforacion del anterior item';
                }

             
             /**************end hco*/


            /////////////////////////////////////////////////////

        }else{
            /////////////////////////////////////////////////////

         if($validar==1){
                $val = $this->em->getRepository('admhcBundle:Adm_'.strtolower($entityanterior))
                ->findOneBy(array('idAdm'.$entityanterior=>$idAdmision,'numHco'=>$numHco,'status'.$entityanterior=>1));

            $array['message']='Por favor ingrese informacion de el anterior item para acceder a esta sección';


                if($val){
                    $array['status']=1; 

                    $val2 = $this->em->getRepository('admhcBundle:Adm_'.strtolower($entitysiguiente))
                    ->findOneBy(array('idAdm'.$entitysiguiente=>$idAdmision,'numHco'=>$numHco,'status'.$entitysiguiente=>1));

                    if($val2){
                        $array['existedatos']=1; 
                        $array['id']=$val2->getId(); 
                        
                        if($valid->getEstadoAdm()== 'ACTIVO'){
                            $array['activo']=1; 

                        }else{
                            $array['activo']=0; 
                        }

                    }else{
                        $array['existedatos']=0;                 
                        if($valid->getEstadoAdm()== 'ACTIVO'){
                            $array['activo']=1; 
                        }else{
                            $array['activo']=0; 
                        }
                    }

                }else{
                    $array['status']=0;
                    $array['message']='No existen datos para esta sección';
 
                }

            }

             if($validar==0){
                $array['status']=1;
                $array['message']='Por favor ingrese informacion de el anterior item para acceder a esta sección';

                    $val2 = $this->em->getRepository('admhcBundle:Adm_'.strtolower($entitysiguiente))
                    ->findOneBy(array('idAdm'.$entitysiguiente=>$idAdmision,'numHco'=>$numHco,'status'.$entitysiguiente=>1));

                    if($val2){
                        $array['existedatos']=1; 
                        $array['id']=$val2->getId(); 
                        
                        if($valid->getEstadoAdm()== 'ACTIVO'){
                            $array['activo']=1; 

                        }else{
                            $array['activo']=0; 
                        }

                    }else{
                        $array['existedatos']=0;                 
                        if($valid->getEstadoAdm()== 'ACTIVO'){
                            $array['activo']=1; 
                        }else{
                            $array['activo']=0; 
                            $array['message']='No existen datos para esta sección';
                        }
                    }
             }



            /////////////////////////////////////////////////////            
        }


        return $array;

    }

    /*******************************end validaciones historia clinica***********************/
    /*******************************end validaciones historia clinica***********************/
    /*******************************end validaciones historia clinica***********************/

public function existefirma($id,$type){
    
    if(file_exists ( $_SERVER['DOCUMENT_ROOT']."/repository/".$type."/".$id.".png" )){
        return 1;
    }else{
        return 0;
    }
    
}

public function ruta(){
    return $_SERVER['DOCUMENT_ROOT'];
}

public function base64ToImg($base64)
{
    $img_str = 'image/png;base64,'.$base64;
    $img_data = explode(";",$img_str);
    $type_img = $img_data[0];
    $final_img = explode(",",$img_data[1]);
    header("Content-type:".$type_img);

    return base64_decode($final_img[1]);
}

public function ByUser($id){
   return $this->em->getRepository('cfguserBundle:Cfg_user')
                    ->find($id);

}

public function blobImage($blob){
    return base64_encode(stream_get_contents($blob));
}

}