<?php
       
      // src/Services/Utils/Ldap.php
       
      namespace Services\Utils;
 
      use Symfony\Component\DependencyInjection\ContainerInterface;
      use Symfony\Component\HttpFoundation\Request;
      use Symfony\Component\HttpFoundation\Response;
 
      class Ldap
      {
 
          private $em, $request, $container;
          private $strLdapServer, $strLdapDN;
          private $objLdapBind, $strLdapFilter, $strLdapDC, $objLdapConnection;
          private $arrLoginResult, $strUserEmail, $strUserPasswd;
 
          public function __construct(ContainerInterface $container, Request $request)
          {
              $this->request            = $request;
              $this->container    = $container;
 
              // LDAP CONFIG
              $this->strLdapServer     = "172.16.50.10";
              $this->strLdapDN    = "DomainName";
              $this->strLdapDC    = "dc=DcName,dc=local";
 
              // init vars
              $this->objLdapBind        = false;
              $this->objLdapConnection = false;
          }
 
          // Load LDAP config
          private function loadLdapConfig()
          {
            $this->strLdapFilter = "(sAMAccountName=" . $this->strUserEmail . ")";
            $this->strLdapServer = "ldap://" . $this->strLdapServer;
            $this->strLdapDN     = $this->strLdapDN . "\\" . $this->strUserEmail;
          }
 
          // Connects to LDAP server
          private function connectToLdapServer()
          {
            $this->objLdapConnection = ldap_connect($this->strLdapServer);
            ldap_set_option($this->objLdapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($this->objLdapConnection, LDAP_OPT_REFERRALS, 0);
            $this->objLdapBind = @ldap_bind($this->objLdapConnection, $this->strLdapDN, $this->strUserPasswd);
          }
 
          // Get username and password form login form
          private function getLdapUsernameAndPassword()
          {
            $this->strUserEmail  = $this->request->request->get('email');
            $this->strUserPasswd = $this->request->request->get('password');
 
            if( ! empty($this->strUserEmail) && ! empty($this->strUserPasswd))
            {
                  $this->arrLoginResult['USER_EMAIL'] = $this->strUserEmail;
                  $this->arrLoginResult['PASSWORD']      = $this->strUserPasswd;
 
                  // get only username, deleting all data after @
                  if(preg_match('/@/', $this->strUserEmail))
                  {
                        $arrUserData = explode("@", $this->strUserEmail);
                        $this->strUserEmail = $arrUserData[0];
                  }
            }
            else
            {
                  $this->arrLoginResult['ERROR'] = "EMPTY_CREDENTIALS";
            }
          }
 
          // check ldap login with username and password
          public function checkLdapLogin()
          {
            $this->arrLoginResult = array(
                              'LOGIN'      => 'ERROR', 
                              'ERROR'      => 'INIT',
                              'USER_EMAIL' => NULL,
                              'PASSWORD'   => NULL,
                              'USERNAME'   => NULL
                        );
 
            // get username and password
            $this->getLdapUsernameAndPassword();
 
            if( ! empty($this->strUserEmail) && ! empty($this->strUserPasswd) )
            {
                  // load LDAP config
                  $this->loadLdapConfig();
 
                  // connect to server
                  $this->connectToLdapServer();
 
                  // check connection result
                  if($this->objLdapBind)
                  {
                        // login ok
                        $this->arrLoginResult['LOGIN'] = "OK";
 
                        // get ldap response
                        $result = ldap_search($this->objLdapConnection, $this->strLdapDC, $this->strLdapFilter);
 
                        // sort ldap results
                          ldap_sort($this->objLdapConnection, $result, "sn");
 
                          // get user info
                          $info = ldap_get_entries($this->objLdapConnection, $result);
 
                          // get user info
                          $this->arrLoginResult['USERNAME'] = ! empty($info[0]['name'][0]) ? $info[0]['name'][0] : NULL;
 
                          // close ldap connection
                          @ldap_close($this->objLdapConnection);
 
                          // login user
                          $objUserServ = $this->container->get('userManager');
                          $objUserServ->loginAction($this->strUserEmail);
                  }
                  else
                  {
                        $this->arrLoginResult['ERROR'] = 'INVALID_CREDENTIALS';
                  }
            }
            return json_encode($this->arrLoginResult);
          }
      }