<?php

namespace start\homeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return new Response("Error");

    }

    public function tutorialesAction()
    {
        return new Response("Error");

    }    

    public function huelladigitalAction()
    {
        return new Response("Error");

    }

    public function firmadoraAction(){
        return new Response("Error");
       
    }

    public function soporteAction(){
        return new Response("Error");
       
    }    
}
