import {Injectable} from '@angular/core';
import {Http, Response, Headers,RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';
import {AbstractControl} from '@angular/forms';
import { Acceso } from './../../src/app/acceso';
import { Router } from '@angular/router';


declare var $ :any;
declare var require: any;




@Injectable()
export class Services{
	public url: string;
	headers: Headers = new Headers;
	public respuesta;
	acceso:Acceso;

	headerslogin: Headers = new Headers;

	constructor(private _router: Router,private _http:Http){
		this.url = GLOBAL.url;
		this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Content-Type', 'XMLHttpRequest');


        
	}

	getLogin(json){
		
		const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		const options = new RequestOptions({ headers: headers });
		const params = new URLSearchParams();
		params.append('nameUser', json.nameUser);
		params.append('passwdUser', json.passwdUser);
		return this._http.post(this.url+"login", params, options).map(res => res.json());;
	}

    logout() {
    	var token = localStorage.getItem('id_token');
                        localStorage.removeItem('id_token');
                        localStorage.removeItem('info');    	
        this.headers.append('Authorization', 'Bearer ' +token);

		return this._http.get(this.url+'logout',{headers:this.headers})
		                 .map(res => res.json());     

    }

	getPostObject(ruta,json){
		

        let params = "json="+JSON.stringify(json);
		let headers_local = new Headers;
        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');         

		return this._http.post(this.url+ruta, params,{headers:headers_local})
		                 .map(res => res.json());
	}    


	getAccess(modulo,accion){

		const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		const options = new RequestOptions({ headers: headers });
		const params = new URLSearchParams();

			params.append('token', localStorage.getItem('token'));
			params.append('module', modulo);
			params.append('accion', accion);

			return	 this._http.post(this.url+'verifyaccess', params, options).map(res => 
				 res.json()
			);	

	}

	getAccessSystem(){
		if(localStorage.getItem('info')== undefined || localStorage.getItem('info')== null || 
		localStorage.getItem('token')== undefined || localStorage.getItem('token')== null ){
			return false;
		}	
		return true;
		
	}


	makeFileRequest(ruta: string, params: Array<string>, files: Array<File>,carpeta:string,image:string){
		return new Promise((resolve, reject)=>{
			var formData: any = new FormData();
			var xhr = new XMLHttpRequest();

			for(var i = 0; i < files.length; i++){
				formData.append('uploads[]', files[i], files[i].name);
			}
			formData.append('carpeta', carpeta);
			formData.append('image', image);

			xhr.onreadystatechange = function(){
				if(xhr.readyState == 4){
					if(xhr.status == 200){
						resolve(JSON.parse(xhr.response));
					}else{
						reject(xhr.response);
					}
				}
			};

			xhr.open("POST", this.url+ruta, true);
	//xhr.setRequestHeader("Authorization","Bearer "+localStorage.getItem('id_token'));
  // xhr.setRequestHeader('Content-Type','multipart/form-data');
   // xhr.setRequestHeader("ContentType", "image/jpeg");
//xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");


			xhr.send(formData);
		});
	}
















/*



	getObjet(ruta){
		let headers_local = new Headers;
        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
        headers_local.append('Content-Type', 'XMLHttpRequest');
        headers_local.append('Content-Type', 'application/json');		
				return this._http.get(this.url+'/'+ruta,{headers:headers_local}).map(res => res.json());
	}

	getPostObject(ruta,json){
		let headers_local = new Headers;
        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
        headers_local.append('Content-Type', 'XMLHttpRequest');
        headers_local.append('Content-Type', 'application/json');
			return this._http.post(this.url+'/'+ruta,JSON.stringify(json),{headers:headers_local})
		                 .map(res => res.json());		
	}

	getObjetSearchPost(ruta,page,name,type,json){
		let headers_local = new Headers;
        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
        headers_local.append('Content-Type', 'XMLHttpRequest');
        headers_local.append('Content-Type', 'application/json');		
	
		return this._http.post(this.url+'/'+ruta+'?page='+page+'&type='+type+'&name='+name,JSON.stringify(json),{headers:headers_local})
		                 .map(res => res.json());
	}

	getObjetSearch(ruta,page,name,type){
		let headers_local = new Headers;
        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
        headers_local.append('Content-Type', 'XMLHttpRequest');
        headers_local.append('Content-Type', 'application/json');		
		return this._http.get(this.url+'/'+ruta+'?page='+page+'&type='+type+'&name='+name,{headers:headers_local}).map(res => res.json());		
	}	

	setObjet(ruta,json,id){
		let headers_local = new Headers;
        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
        headers_local.append('Content-Type', 'XMLHttpRequest');
        headers_local.append('Content-Type', 'application/json');


		let url= '';
		if(id==null){
			return this._http.post(this.url+'/'+ruta,JSON.stringify(json),{headers:headers_local})
		                 .map(res => res.json());		
		}else{
			return this._http.put(this.url+'/'+ruta+"/"+id,JSON.stringify(json),{headers:headers_local})
		                 .map(res => res.json());			
		}

	}

	setPerfil(ruta,json,id){
		let headers_local = new Headers;
        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
        headers_local.append('Content-Type', 'XMLHttpRequest');
        headers_local.append('Content-Type', 'application/json');

			return this._http.put(this.url+'/'+ruta+"/"+id,JSON.stringify(json),{headers:headers_local})
		                 .map(res => res.json());	

	}	






	delObjet(ruta,id){
		let headers_local = new Headers;
        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
        headers_local.append('Content-Type', 'XMLHttpRequest');
        headers_local.append('Content-Type', 'application/json');
    	return this._http.delete(this.url+"/"+ruta+"/"+id, { headers:headers_local })
                  .map((resp:Response)=>resp.json());
		
	}		



	getObjetId(ruta,id){
		let headers_local = new Headers;
        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
        headers_local.append('Content-Type', 'XMLHttpRequest');
        headers_local.append('Content-Type', 'application/json');
		return this._http.get(this.url+'/'+ruta+'/'+id+"/edit",{headers:headers_local})
		                 .map(res => res.json());
		
	}	






    getToken(){
    	let token = localStorage.getItem('id_token');
    	if(token == null){
    		return null;
    	}else{
    		return token;
    	}
    }
 
 	getFacebook(){
		return this._http.post(this.url+'/auth/facebook',{headers:this.headers})
		                 .map(res => res.json()); 		
 	}



	// files json
	  getJsonGeneral(){
	    var obj = JSON.stringify(JsonGeneralFile);
	    return     JsonGeneralFile;
	  }



	  getPermiso(modulo){

		let myObj = JSON.parse(localStorage.getItem("info"));

		
			let headers_local = new Headers;
	        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
	        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
	        headers_local.append('Content-Type', 'XMLHttpRequest');
	        headers_local.append('Content-Type', 'application/json');

	         return this._http.post(this.url+'/permisos/moduleuser',JSON.stringify({'type_user':myObj['type_user'],'rol':myObj['id_rol_user'],'module':modulo}),{headers:headers_local})
			                 .map(res => res.json());
	  }

	  getPermisoxmodulo(modulo){

		let myObj = JSON.parse(localStorage.getItem("info"));

		      let miObjeto = new Object();
               miObjeto['type_user'] = myObj['type_user'];
               miObjeto['rol'] = myObj['id_rol_user'];
               miObjeto['module'] =modulo;


			let headers_local = new Headers;
	        headers_local.append('Authorization', 'Bearer ' +localStorage.getItem('id_token'));
	        headers_local.append('Content-Type', 'application/x-www-form-urlencoded');
	        headers_local.append('Content-Type', 'XMLHttpRequest');
	        headers_local.append('Content-Type', 'application/json');

	         return this._http.post(this.url+'/permisos/getmodulos',JSON.stringify(miObjeto),{headers:headers_local})
			                 .map(res => res.json());
	  }	  

	  getUserlogin(){
	  			let myObj = JSON.parse(localStorage.getItem("info"));
	  		if(myObj['type_user']=='ROOT'){
					return 'TODOS';
			}else{
				return null;
			}
	  }

	  */
	
	/******************************************************************/
	/****************************VALIDACIONES**************************/
	/******************************************************************/
	/******************************************************************/
	/******************************************************************/

    MatchPassword(AC: AbstractControl) {
       let password = AC.get('passwdUser').value; // to get value in input tag
       let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
        if(password != confirmPassword) {
            AC.get('confirmPassword').setErrors( {MatchPassword: true} );
        } else {
            return null;
        }
    }

    emailValidatorIn(AC: AbstractControl){
    	let email = AC.get('emailUser').value;
    	let isValid = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(AC.get('emailUser').value); 
    	if(isValid){
    		return null; 
    	}else{
    		AC.get('emailUser').setErrors( {emailValidatorIn: true} );
    	}

    }


}