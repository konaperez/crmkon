export class Permisos{
	constructor(
    	public id:number,
    	public new_permission:string,
		public delete_permission:string,
		public list_permission:string,
		public edit_permission:string,
		public show_permission:string,
		public cause_permission:string,
		public descause_permission:string,
		public visible_permission:string,
		public created_at:string,
		public updated_at:string,
		public id_url_permission:number,
		public id_rol_permission:number

    	
	){}
}