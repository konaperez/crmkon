import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing, appRoutingProviders } from './rrhh.routing';
import {SelectModule} from 'ng2-select';

import { Nomina } from './nomina/nomina';
import {NgxPaginationModule} from 'ngx-pagination';
import { OurpalmCKEditorModule } from 'ngx-ourpalm-ckeditor';
//import { CKEditorModule } from 'ngx-ckeditor';

@NgModule({
  declarations: [
    Nomina,


  ],
  imports: [
  NgxPaginationModule,
    CommonModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    routing,
    ReactiveFormsModule,
    SelectModule,
    OurpalmCKEditorModule
  
  ],
  providers: [appRoutingProviders],
})
export class RrhhModule { }
