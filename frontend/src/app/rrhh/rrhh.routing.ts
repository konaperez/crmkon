import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importar componentes
import { Nomina } from './nomina/nomina';

const RrhhRoutes: Routes = [



    {path: 'rrhh_nomina', component: Nomina},    

    /*
    
    {path: 'cfg_role', component: Role,
        children: [
            { path: 'edit/:type/:id', component: RoleForm },
            { path: 'show/:type/:id', component: RoleForm },
             { path: 'new/:type/:id', component: RoleForm },
        ]
    },

    */



];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(RrhhRoutes);