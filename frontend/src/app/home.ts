import { Component,ViewChild,NgZone } from '@angular/core';
import { Services } from 'services/services';
import { Acceso } from './acceso';

import {GLOBAL} from 'services/global';
declare var $:any;
declare var Pusher: any;

import { NotificationService } from 'ng2-notify-popup';
import { Router } from '@angular/router';
//import { AuthService, AppGlobals } from 'angular2-google-login';
declare var gapi: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.html',
  providers: [Services,NotificationService],
  
})
export class Home {
  title = 'Home';
  public respuesta;
  public info=null;
  permisos:any;
  url = GLOBAL.url;
  urlproject = GLOBAL.urlproject;
  urlassets = GLOBAL.urlassets;
  acceso:any;
  menu:any;

      constructor(private zone: NgZone,private _router: Router,private _service: Services,private notify:NotificationService){
        this._service.getAccessSystem();
      }


ngOnInit() {
     this._service.getAccess('home','home').subscribe(response => {
      this.acceso=response;
      if(this.acceso.success=='OK'){
        this.getMenu();
      }else{
        this._router.navigate(['home']);
      }
     });
    }


getMenu(){
  let miObjetoInfo= new Object();      
  miObjetoInfo['token'] = localStorage.getItem('token'); 
  this._service.getPostObject('getmodules',miObjetoInfo).subscribe(response => {
    console.log(response);
    this.menu=response.modules;
   });
}

salir(){
  localStorage.removeItem('token')
  localStorage.removeItem('info')
  this._router.navigate(['/login']);
  
}




/*********************************************/
/*********************************************/
/*****************NOTIFICACIONES**************/
/*********************************************/
/*********************************************/
/*********************************************/


/*********************************************/
/*********************************************/
/*********************************************/
/*********************************************/
/*********************************************/
/*********************************************/

}