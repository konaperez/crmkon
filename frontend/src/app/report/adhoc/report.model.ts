export class ReportModel {
	constructor(
		public nameReport:string,
        public fechaInicio:string,
    	public fechaFin:string,     
	){}
}
