import { Component,OnInit } from '@angular/core';
import { Services } from 'services/services';
import { Router, ActivatedRoute,Params } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { ReportModel } from './report.model';
import { NotificationService } from 'ng2-notify-popup';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {GLOBAL} from 'services/global';

declare var $ :any;

@Component({
  selector: 'app-adhoc',
  templateUrl: './adhoc.html',
  providers: [Services,NotificationService]  
})
export class Adhoc {
  title = 'AD-HOC';
  in:ReportModel;
  err:ReportModel;
  loading=true;
  search;
  public form: FormGroup;
  id;
  type;
  respuesta:any;
  acceso:any;
  entidades:any;
  body=null;
  head:any;
  carga_tabla=false;

  public url = GLOBAL.url;
  public urlproject = GLOBAL.urlproject;
  public urlassets = GLOBAL.urlassets;
  
  constructor(public fb: FormBuilder,private _route: ActivatedRoute,private _router: Router,private _service: Services,private notify:NotificationService){
    this.in =new ReportModel(null,null,null);
    this.err =new ReportModel(null,null,null);
    this.loading=true; 
    this.search=''; 
      
       this.form = this.fb.group({
        'nameReport': ['', [Validators.required]],
        'fechaInicio': ['', [Validators.required]],
        'fechaFin': ['', [Validators.required]]
      });
       
 
   
}

ngOnInit() {
  this.getAccess();

}

getAccess(){
  this._service.getAccess('cfg_report',null).subscribe(response => {
    this.acceso=response;
    if(this.acceso.auth.success=='OK'){
       if(this.acceso.permisos[0].show_permission=='SI'){
        this.getentidades();
      }else{
        this._router.navigate(['/notfound']);
       }
    }else{
      location.reload();  
    }
   }); 
}

onSubmit(){
  this._service.getPostObject('cfg_report/generar',this.in).subscribe(response => {
    this.respuesta=response;
    console.log(this.respuesta);
    this.carga_tabla=false;
    if(this.respuesta.success == 'OK'){
      this.carga_tabla=true;
      setTimeout(function(){
        $(document).ready(function() {
          $('#tabla-report').DataTable( {
              dom: 'Bfrtip',
              buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
              ]
          });
        })
  
      },1000);
       this.head=this.respuesta.head;
      this.body=this.respuesta.body;
    }else{
      this.err=this.respuesta.errores[0];
      this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
    }
  }); 
}

onsubmit2(){
  let url =this.url+"cfg_report/generarexcel?id="+this.in.nameReport+"&fin="+this.in.fechaInicio+"&ffn="+this.in.fechaFin;
  window.open(url);

}

getentidades(){
  this._service.getPostObject('cfg_report/entidades',{'tipo':'AD-HOC'}).subscribe(response => {
    this.respuesta=response;
        this.loading=false;      
        this.entidades = this.respuesta.data; 
   }); 
}


}
