import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importar componentes
import { Adhoc } from './adhoc/adhoc';

const ReportRoutes: Routes = [



    {path: 'cfg_report', component: Adhoc},    

    /*
    
    {path: 'cfg_role', component: Role,
        children: [
            { path: 'edit/:type/:id', component: RoleForm },
            { path: 'show/:type/:id', component: RoleForm },
             { path: 'new/:type/:id', component: RoleForm },
        ]
    },

    */



];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(ReportRoutes);