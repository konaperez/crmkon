import { Component,ViewChild,NgZone } from '@angular/core';
import { Services } from 'services/services';
import {GLOBAL} from 'services/global';
import {LoginModel} from './login.model';

declare var $:any;
declare var Pusher: any;

import { NotificationService } from 'ng2-notify-popup';
import { Router } from '@angular/router';
declare var gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.html',
  providers: [Services,NotificationService],  
})
export class Login {
  title = 'app';
  public respuesta;
  public access=null;
  public info=null;
  public url = GLOBAL.url;
  public urlproject = GLOBAL.urlproject;
  public urlassets = GLOBAL.urlassets;
  public login:LoginModel;
  obj= new Object();
  constructor(private zone: NgZone,private _router: Router,private _service: Services,private notify:NotificationService){
    this.login=new LoginModel(null,null);
  }


ngOnInit() {
      //this._service.getAccess('home','home');
    }

onSubmit(){
              console.log(this.login);
               this._service.getLogin(this.login).subscribe(response => {
               this.respuesta=response;
               console.log('ingreso');

               console.log(this.respuesta);

                   if(this.respuesta.auth.success == 'OK'){
                     localStorage.setItem('info',JSON.stringify(this.respuesta.auth.data));
                     localStorage.setItem('token',this.respuesta.auth.token);

                     this.access=localStorage.getItem("token");
                     this.info=localStorage.getItem("info");
                     console.log(this.info);
                     this._router.navigate(['/home']);


                   	/*
                     localStorage.setItem('id_token', this.respuesta.token);
                     let info = localStorage.getItem("info");
                      this.info = JSON.parse(info);
                      //console.log(this.info);
                      */

                     //location.reload();
                   }else{
                     localStorage.setItem('access','0');
                     this.access=localStorage.getItem("access");

                     //this.err=this.respuesta.status;
                     this.notify.show('Usuario y/o password incorrectos', { position:'top', duration:'2000', type: 'error' });                          
                   }

               },error => {
                     this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
                });  
                	  
}


logout_salir(){
   localStorage.removeItem('token');  
   localStorage.removeItem('info');  
}    




/*********************************************/
/*********************************************/
/*****************NOTIFICACIONES**************/
/*********************************************/
/*********************************************/
/*********************************************/


/*********************************************/
/*********************************************/
/*********************************************/
/*********************************************/
/*********************************************/
/*********************************************/

}