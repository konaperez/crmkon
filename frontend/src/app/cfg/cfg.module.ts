import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing, appRoutingProviders } from './cfg.routing';
import {SelectModule} from 'ng2-select';

import { Profile } from './profile/profile';
import { Role } from './role/role';
import { RoleForm } from './role/role.form';
import { User } from './user/user';
import { UserForm } from './user/user.form';
import { Entity } from './entity/entity';
import { EntityForm } from './entity/entity.form';
import { Permiso } from './permisos/permiso';
import {NgxPaginationModule} from 'ngx-pagination';
import { OurpalmCKEditorModule } from 'ngx-ourpalm-ckeditor';
//import { CKEditorModule } from 'ngx-ckeditor';

@NgModule({
  declarations: [
  
  Role,
  RoleForm,
  User,
  UserForm,
  Entity,
  EntityForm,
  Permiso,
  Profile


  ],
  imports: [
  NgxPaginationModule,
    CommonModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    routing,
    ReactiveFormsModule,
    SelectModule,
    OurpalmCKEditorModule
  
  ],
  providers: [appRoutingProviders],
})
export class CfgModule { }
