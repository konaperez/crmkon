import { Component,OnInit } from '@angular/core';
import { Services } from 'services/services';
import { Router, ActivatedRoute,Params } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { NotificationService } from 'ng2-notify-popup';
import { ProfileModel } from './profile.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {GLOBAL} from 'services/global';
declare var document:any;
declare var $ :any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.html',
  providers: [Services,NotificationService]  

})
export class Profile {
  title = 'Mi perfil';
  in:ProfileModel;
  err:ProfileModel;
  loading=true;
  search;
  public form: FormGroup;
  id;
  type;
  respuesta:any;
  acceso:any;
  entidades:any;
  public files;

  public url = GLOBAL.url;
  public urlproject = GLOBAL.urlproject;
  public urlassets = GLOBAL.urlassets;

    info:any;

  constructor(public fb: FormBuilder,private _route: ActivatedRoute,private _router: Router,private _service: Services,private notify:NotificationService){
    this.in =new ProfileModel(null,null,null);
    this.err =new ProfileModel(null,null,null);
    this.loading=true; 
    this.search=''; 

      this.form =  fb.group({
        'passwdUser': ['', [Validators.required]],
        'imgUser': ['', ''],
        'confirmPassword': ['', [Validators.required]]
        },{
          validator: this._service.MatchPassword 
        });  
        
        if(localStorage.getItem('info')!= undefined || localStorage.getItem('info')!= null  ){
            this.info = $.parseJSON( localStorage.getItem('info') );;
            this.id=this.info['idUser'];
          }        

}
 

ngOnInit() {
  this.getAccess();

}

getAccess(){
  this._service.getAccess('cfg_profile',null).subscribe(response => {
    this.acceso=response;
    console.log(this.acceso);
    if(this.acceso.auth.success=='OK'){
       if(this.acceso.permisos[0].show_permission=='SI'){
        this.data();
      }else{
        this._router.navigate(['/notfound']);
       }
    }else{
      location.reload();  
    }
   }); 
}

data(){
    this.getId();
}

onSubmit(){
  if(this.files && this.files.length >= 1){
    this._service.makeFileRequest('cfg_user/filesadd', [], this.files,'users',this.in.imgUser).then((response) => {
      this.respuesta=response;
      if(this.respuesta.success == 'OK'){
        this.in.imgUser = this.respuesta.image_upload;        
         this.saveData();
      }else{
         this.notify.show("HA OCURRIDO UN FALLO", { position:'top', duration:'2000', type: 'error' });
      }
    }, (error) =>{
      console.log(error);
    });
  }else{
    this.saveData();  
  }
}

saveData(){
  console.log(this.in);
  this._service.getPostObject('cfg_user/updateperfil',this.in).subscribe(response => {
  this.respuesta=response;
  console.log(this.respuesta);
  if(this.respuesta.success == 'OK'){
    this.notify.show('PROCESO CORRECTO', { position:'top', duration:'2000', type: 'success' }); 
    this._router.navigate(['/']);
  }else{
    this.err=this.respuesta.errores[0];
    this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
  }
 }); 
}


getId(){
  this._service.getPostObject('cfg_user/editperfil/'+this.id,null).subscribe(response => {
    this.respuesta=response;
    if(this.respuesta.success == 'OK'){
      this.loading=false;      
      this.in = this.respuesta.data;
    }else{
      this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
    }
   });   
}


makeToFiles(fileInput:any){
  this.previewFile();
  this.files = fileInput.target.files;
} 

    previewFile() {
    var preview = document.querySelector('img');
    var file = document.querySelector('input[type=file]').files[0];

    var reader = new FileReader();
    
    reader.onloadend = function () {
        $('#imagen-blob').attr("src", reader.result);
        $('.target-preview').attr('src', reader.result);
        $('.target-preview').css({ 'display': 'block','width':'100%'});
    }
    if (file) {
        reader.readAsDataURL(file);
    }
}


}
