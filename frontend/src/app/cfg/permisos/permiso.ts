import { Component,OnInit } from '@angular/core';
import { Services } from 'services/services';
import { Permisos } from 'services/permisos';
import { Router, ActivatedRoute,Params } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { NotificationService } from 'ng2-notify-popup';
import {GLOBAL} from 'services/global';
import { PaginadorModel } from 'services/paginador.model';

declare var $ :any;

@Component({
  selector: 'app-permiso',
  templateUrl: './permiso.html',
  providers: [Services,NotificationService]
})
export class Permiso {
  title = 'Permisos';
  public permisos:Permisos;
  public loading : boolean;
  public id:number;
  public list;
  public respuesta;
  //errors
  public search;
  public permission = new Array();
  public listpermission;

  public url = GLOBAL.url;
  public urlproject = GLOBAL.urlproject;
  public urlassets = GLOBAL.urlassets;

  acceso:any;
  public paginador:PaginadorModel;

    constructor(private _route: ActivatedRoute,private _router: Router,private _service: Services,private notify:NotificationService){
        this.loading=true; 
        this.search=''; 
        this.id = Number(this._route.snapshot.params['id']);

    }

    ngOnInit() {
      this.getAccess();

      //this.permisosModulo('cfg_permisos');
       
    }

    getAccess(){
      this._service.getAccess('cfg_permission',null).subscribe(response => {
        this.acceso=response;
        if(this.acceso.auth.success=='OK'){
           if(this.acceso.permisos[0].visible_permission=='SI'){
            this.cargar(1,'','');
           }else{
            this._router.navigate(['/notfound']);
           }
        }else{
          location.reload();  
        }
       });
    }

    cargar(page,name,type){
       
      this._service.getPostObject('cfg_permission/listpermission/'+this.id,null).subscribe(response => {
        console.log(response);
        this.listpermission=response.data;
        this.loading=false;
       }); 
          
   }

   onSubmit(type:any,val,e){
    let valor = 'NO';
    if(e.target.checked){
      valor = 'SI';
    }
        let json = {
           type: type,
           estado: valor,
           url: val,
        };
        this._service.getPostObject('cfg_permission/new/'+this.id,json).subscribe(response => {
          console.log(response);
          this.respuesta=response;
         });         
     }

    /*
  permisosModulo(modulo){

       this._service.getPermiso(modulo).subscribe(response => {
       this.respuesta=response;
           if(this.respuesta.success == 'success'){
               this.permisos=this.respuesta.data;
               
               if(this.permisos[0].visible_permission=='NO'){
                  this._router.navigate(['/']);
               }else{
                 this.cargarpermisos(1,null,null);
               }

           }else{
             this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                     
           }
       },error => {
             this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });
            }       

       );  
  }
*/

}