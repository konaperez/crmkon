import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importar componentes
import { Role } from './role/role';
import { RoleForm } from './role/role.form';
import { User } from './user/user';
import { UserForm } from './user/user.form';
import { Entity } from './entity/entity';
import { EntityForm } from './entity/entity.form';
import { Permiso } from './permisos/permiso';
import { Profile } from './profile/profile';

const CfgRoutes: Routes = [



    {path: 'cfg_role', component: Role},
    {path: 'cfg_role/:type/:id', component: RoleForm},
    {path: 'cfg_user', component: User},
    {path: 'cfg_user/:type/:id', component: UserForm},
    {path: 'cfg_entity', component: Entity},
    {path: 'cfg_entity/:type/:id', component: EntityForm},    
    {path: 'permisos/:id', component: Permiso},    
    {path: 'profile', component: Profile},    

    /*
    
    {path: 'cfg_role', component: Role,
        children: [
            { path: 'edit/:type/:id', component: RoleForm },
            { path: 'show/:type/:id', component: RoleForm },
             { path: 'new/:type/:id', component: RoleForm },
        ]
    },

    */



];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(CfgRoutes);