import { Component,OnInit } from '@angular/core';
import { Services } from 'services/services';
import { Router, ActivatedRoute,Params } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { EntityModel } from './entity.model';
import { NotificationService } from 'ng2-notify-popup';
import { Entity } from './entity';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {GLOBAL} from 'services/global';
declare var document:any;
declare var $ :any;

@Component({
  selector: 'app-entity-form',
  templateUrl: './entity.form.html',
  providers: [Services,NotificationService,Entity]  

})
export class EntityForm {
  title = 'app';
  in:EntityModel;
  err:EntityModel;
  loading=true;
  search;
  public form: FormGroup;
  id;
  type;
  respuesta:any;
  acceso:any;
  entidades:any;
  public files;

  public url = GLOBAL.url;
  public urlproject = GLOBAL.urlproject;
  public urlassets = GLOBAL.urlassets;

  constructor(public fb: FormBuilder,private component: Entity,private _route: ActivatedRoute,private _router: Router,private _service: Services,private notify:NotificationService){
    this.in =new EntityModel(null,null,null,null,null,null,null,null,null,null,null,null,null);
    this.err =new EntityModel(null,null,null,null,null,null,null,null,null,null,null,null,null);
    this.loading=true; 
    this.search=''; 

      this.form =  fb.group({
        'nameEntity': ['', [Validators.required]],
        'addressEntity': ['', [Validators.required]],
        'imgEntity': ['', ''],
        'phoneEntity': ['', [Validators.required]],
        'obsEntity': ['', ''],
        'webEntity': ['', ''],
        'cityEntity': ['', [Validators.required]],
        'departamentoEntity': ['', [Validators.required]],
        'actEntity': ['', [Validators.required]],
        'sedeEntity': ['', [Validators.required]],
        'identEntity': ['', ''],
        'emailEntity': ['', [Validators.required, Validators.email]],
        });      

    this.id = Number(this._route.snapshot.params['id']);
    this.type = this._route.snapshot.params['type'];
 
}
 

ngOnInit() {
  this.getAccess();

}

getAccess(){
  this._service.getAccess('cfg_entity',null).subscribe(response => {
    this.acceso=response;
    if(this.acceso.auth.success=='OK'){
       if(this.acceso.permisos[0].show_permission=='SI'){
        this.data();
      }else{
        this._router.navigate(['/notfound']);
       }
    }else{
      location.reload();  
    }
   }); 
}

data(){
  if(this.type=='new'){
    this.title='Formulario de Registro';
    this.loading=false;
  }else{
    this.getId();
    
  }
}

onSubmit(){
  if(this.files && this.files.length >= 1){
    this._service.makeFileRequest('cfg_user/filesadd', [], this.files,'entity',this.in.imgEntity).then((response) => {
      this.respuesta=response;
      if(this.respuesta.success == 'OK'){
        this.in.imgEntity = this.respuesta.image_upload;        
         this.saveData();
      }else{
         this.notify.show("HA OCURRIDO UN FALLO", { position:'top', duration:'2000', type: 'error' });
      }
    }, (error) =>{
      console.log(error);
    });
  }else{
    this.saveData();  
  }
}

saveData(){
  if(this.id != 0){
    this.in.id = this.id;
  }
  
  this._service.getPostObject('cfg_entity/new',this.in).subscribe(response => {
  this.respuesta=response;
  if(this.respuesta.success == 'OK'){
    this.notify.show('PROCESO CORRECTO', { position:'top', duration:'2000', type: 'success' }); 
    this._router.navigate(['cfg_entity']);
  }else{
    this.err=this.respuesta.errores[0];
    this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
  }
 }); 
}


getId(){
  this._service.getPostObject('cfg_entity/edit/'+this.id,null).subscribe(response => {
    this.respuesta=response;
    if(this.respuesta.success == 'OK'){
      this.loading=false;      
      this.in = this.respuesta.data;
    }else{
      this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
    }
   });   
}




makeToFiles(fileInput:any){
  this.previewFile();
  this.files = fileInput.target.files;
} 

    previewFile() {
    var preview = document.querySelector('img');
    var file = document.querySelector('input[type=file]').files[0];

    var reader = new FileReader();
    
    reader.onloadend = function () {
        $('#imagen-blob').attr("src", reader.result);
        $('.target-preview').attr('src', reader.result);
        $('.target-preview').css({ 'display': 'block','width':'100%'});
    }
    if (file) {
        reader.readAsDataURL(file);
    }
}


}
