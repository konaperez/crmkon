export class EntityModel {
	constructor(
    	public id:number,
		public nameEntity:string,
        public addressEntity:string,
    	public phoneEntity:string,
    	public emailEntity:string,
    	public obsEntity:string,
    	public webEntity:string,
    	public cityEntity:string,
    	public departamentoEntity:string,
    	public imgEntity:string,
    	public actEntity:string,        
    	public sedeEntity:string,        
    	public identEntity:string        

    ){}
}
