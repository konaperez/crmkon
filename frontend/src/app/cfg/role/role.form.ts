import { Component,OnInit } from '@angular/core';
import { Services } from 'services/services';
import { Router, ActivatedRoute,Params } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { RoleModel } from './role.model';
import { NotificationService } from 'ng2-notify-popup';
import { Role } from './role';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {GLOBAL} from 'services/global';

declare var $ :any;

@Component({
  selector: 'app-role-form',
  templateUrl: './role.form.html',
  providers: [Services,NotificationService,Role]  
})
export class RoleForm {
  title = 'Rol';
  in:RoleModel;
  err:RoleModel;
  loading=true;
  search;
  public form: FormGroup;
  id;
  type;
  respuesta:any;
  acceso:any;


  constructor(public fb: FormBuilder,private component: Role,private _route: ActivatedRoute,private _router: Router,private _service: Services,private notify:NotificationService){
    this.in =new RoleModel(0,"","");
    this.err =new RoleModel(0,"","");
    this.loading=true; 
    this.search=''; 
      
       this.form = this.fb.group({
        'nameRole': ['', [Validators.required]],
        'actRole': ['', [Validators.required]]
      });
       

    this.id = Number(this._route.snapshot.params['id']);
    this.type = this._route.snapshot.params['type'];
   
}

ngOnInit() {
  this.getAccess();

}

getAccess(){
  this._service.getAccess('cfg_role',null).subscribe(response => {
    this.acceso=response;
    if(this.acceso.auth.success=='OK'){
       if(this.acceso.permisos[0].show_permission=='SI'){
        this.data();
      }else{
        this._router.navigate(['/notfound']);
       }
    }else{
      location.reload();  
    }
   }); 
}

data(){
  if(this.type=='new'){
    this.title='Formulario de Registro';
    this.loading=false;
  }else{
    this.getId();
  }
}

onSubmit(){
  if(this.id != 0){
    this.in.id = this.id;
  }
  this._service.getPostObject('cfg_role/new',this.in).subscribe(response => {
  this.respuesta=response;
  if(this.respuesta.success == 'OK'){
    this.notify.show('PROCESO CORRECTO', { position:'top', duration:'2000', type: 'success' }); 
    this._router.navigate(['cfg_role']);
  }else{
    this.err=this.respuesta.errores[0];
    this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
  }
 }); 
}


getId(){
  this._service.getPostObject('cfg_role/edit/'+this.id,null).subscribe(response => {
    this.respuesta=response;
    console.log(this.respuesta);
    if(this.respuesta.success == 'OK'){
      this.loading=false;      
      this.in = this.respuesta.data;
    }else{
      this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
    }
   });   
}

}
