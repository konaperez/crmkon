export class UserModel {
	constructor(
    	public id:number,
		public nameapeUser:string,
        public nameUser:string,
    	public imgUser:string,
    	public passwdUser:string,
    	public emailUser:string,
    	public lastaccessUser:string,
    	public actUser:string,
    	public typeUser:string,
    	public idEntityUser:number,
		public idRoleUser:number, 
		public idTypedocUser:number, 
		public numidUser:string,        
	){}
}
