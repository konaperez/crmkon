import { Component,OnInit } from '@angular/core';
import { Services } from 'services/services';
import { Router, ActivatedRoute,Params } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { UserModel } from './user.model';
import { NotificationService } from 'ng2-notify-popup';
import { User } from './user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {GLOBAL} from 'services/global';
declare var document:any;
declare var $ :any;

@Component({
  selector: 'app-user-form',
  templateUrl: './user.form.html',
  providers: [Services,NotificationService,User]  

})
export class UserForm {
  title = 'app';
  in:UserModel;
  err:UserModel;
  loading=true;
  search;
  public form: FormGroup;
  id;
  type;
  respuesta:any;
  acceso:any;
  entidades:any;
  public files;

  public url = GLOBAL.url;
  public urlproject = GLOBAL.urlproject;
  public urlassets = GLOBAL.urlassets;

  constructor(public fb: FormBuilder,private component: User,private _route: ActivatedRoute,private _router: Router,private _service: Services,private notify:NotificationService){
    this.in =new UserModel(null,null,null,null,null,null,null,null,null,null,null,null,null);
    this.err =new UserModel(null,null,null,null,null,null,null,null,null,null,null,null,null);
    this.loading=true; 
    this.search=''; 

      this.form =  fb.group({
        'nameapeUser': ['', [Validators.required]],
        'nameUser': ['', [Validators.required]],
        'imgUser': ['', ''],
        'passwdUser': ['', [Validators.required]],
        'confirmPassword': ['', [Validators.required]],
        'actUser': ['', [Validators.required]],
        'idEntityUser': ['', [Validators.required]],
        'idRoleUser': ['', [Validators.required]],
        'idTypedocUser': ['', [Validators.required]],
        'numidUser': ['', [Validators.required]],                
        'emailUser': ['', [Validators.required, Validators.email]],
        }, {
          validator: this._service.MatchPassword 
        });      

    this.id = Number(this._route.snapshot.params['id']);
    this.type = this._route.snapshot.params['type'];
 
    
/*******************************/
var options = {
  locale        : { emptyTitle: 'Seleccione...'},
  log           : 3,
  preprocessData: function (data) {
      var i, l = data.length, array = [];
      if (l) {
          for (i = 0; i < l; i++) {
              array.push($.extend(true, data[i], {
                  text : data[i].Name,
                  value: data[i].Id,
              }));
          }
      }
      return array;
  }
};

setTimeout(function(){
  $(".selectpicker")
  .selectpicker()
  .filter(".with-ajax")
  .ajaxSelectPicker(options);
  $("select").trigger("change");
},1000);


function chooseSelectpicker(index, selectpicker) {
$(selectpicker).val(index);
$(selectpicker).selectpicker('refresh');
}
/*********************************/
}
 

ngOnInit() {
  this.getAccess();

}

getAccess(){
  this._service.getAccess('cfg_role',null).subscribe(response => {
    this.acceso=response;
    if(this.acceso.auth.success=='OK'){
       if(this.acceso.permisos[0].show_permission=='SI'){
        this.data();
      }else{
        this._router.navigate(['/notfound']);
       }
    }else{
      location.reload();  
    }
   }); 
}

data(){
  if(this.type=='new'){
    this.title='Formulario de Registro';
    this.getentidades();
  }else{
    this.getentidades();
    
  }
}

onSubmit(){
  if(this.files && this.files.length >= 1){
    this._service.makeFileRequest('cfg_user/filesadd', [], this.files,'users',this.in.imgUser).then((response) => {
      this.respuesta=response;
      if(this.respuesta.success == 'OK'){
        this.in.imgUser = this.respuesta.image_upload;        
         this.saveData();
      }else{
         this.notify.show("HA OCURRIDO UN FALLO", { position:'top', duration:'2000', type: 'error' });
      }
    }, (error) =>{
      console.log(error);
    });
  }else{
    this.saveData();  
  }
}

saveData(){
  if(this.id != 0){
    this.in.id = this.id;
  }
  
  this._service.getPostObject('cfg_user/new',this.in).subscribe(response => {
  this.respuesta=response;
  if(this.respuesta.success == 'OK'){
    this.notify.show('PROCESO CORRECTO', { position:'top', duration:'2000', type: 'success' }); 
    this._router.navigate(['cfg_user']);
  }else{
    this.err=this.respuesta.errores[0];
    this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
  }
 }); 
}


getId(){
  this._service.getPostObject('cfg_user/edit/'+this.id,null).subscribe(response => {
    this.respuesta=response;
    if(this.respuesta.success == 'OK'){
      this.loading=false;      
      this.in = this.respuesta.data;
      this.in.passwdUser='';
    }else{
      this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
    }
   });   
}

getentidades(){
  this._service.getPostObject('cfg_user/entidades',null).subscribe(response => {
    this.respuesta=response;
    if(this.respuesta.success == 'OK'){
      if(this.id==0){
        this.loading=false;      
        this.entidades = this.respuesta.data;
      }else{
        this.entidades = this.respuesta.data;
        this.getId(); 
      }
 
    }
   }); 
}


makeToFiles(fileInput:any){
  this.previewFile();
  this.files = fileInput.target.files;
} 

    previewFile() {
    var preview = document.querySelector('img');
    var file = document.querySelector('input[type=file]').files[0];

    var reader = new FileReader();
    
    reader.onloadend = function () {
        $('#imagen-blob').attr("src", reader.result);
        $('.target-preview').attr('src', reader.result);
        $('.target-preview').css({ 'display': 'block','width':'100%'});
    }
    if (file) {
        reader.readAsDataURL(file);
    }
}


}
