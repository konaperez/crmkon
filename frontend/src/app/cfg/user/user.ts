import { Component,OnInit } from '@angular/core';
import { Services } from 'services/services';
import { PaginadorModel } from 'services/paginador.model';

import { Router, ActivatedRoute,Params } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { UserModel } from './user.model';
import { NotificationService } from 'ng2-notify-popup';
import {GLOBAL} from 'services/global';

declare var $ :any;

@Component({
  selector: 'app-user',
  templateUrl: './user.html',
})
export class User {
  title = 'Usuarios';
  UserModel:UserModel;
  loading=true;
  search;
  acceso:any;
  list:any;
  paginador:PaginadorModel;

  public url = GLOBAL.url;
  public urlproject = GLOBAL.urlproject;
  public urlassets = GLOBAL.urlassets;
  
  idDel=0;

    constructor(private _route: ActivatedRoute,private _router: Router,private _service: Services,private notify:NotificationService){
        this.UserModel =new UserModel(null,null,null,null,null,null,null,null,null,null,null,null,null);
        this.paginador =new PaginadorModel(null,null,null);

        this.loading=true; 
        this.search=''; 

        this._service.getAccessSystem();//Metodo1 de Acceso a objeto
    }

    ngOnInit() {
      this.getAccess();
    }

    getAccess(){
      this._service.getAccess('cfg_user',null).subscribe(response => {
        this.acceso=response;
        if(this.acceso.auth.success=='OK'){
           if(this.acceso.permisos[0].visible_permission=='SI'){
            this.cargar(1,'','');
           }else{
            this._router.navigate(['/notfound']);
           }
        }else{
          location.reload();  
        }
       }); 
    }

    cargar(page,name,type){
      this.paginador.name=name;
      this.paginador.type=type;
      this.paginador.page=page;
       
      this._service.getPostObject('cfg_user/show',this.paginador).subscribe(response => {
        console.log(response);
        this.list=response;
        this.loading=false;
       });      
   }

   filter(name,type){
    this.cargar(1,name,type);
    }  

    pageChanged(page){
      this.cargar(page,null,null);
    }

    del(id){
      this.idDel=id;
      
      $('#modal-danger .modal-title').html('Confirmar');
      $('#modal-danger .modal-body').html('<p class="text-center">En verdad desea eliminar el registro?</p>');      
      $('#modal-danger').modal('show');
  
    }

    confirmarSIDelete(){
      this._service.getPostObject('cfg_user/delete/'+this.idDel,null).subscribe(response => {
        if(response.success=='OK'){
          this.cargar(1,null,null);
          $('#modal-danger').modal('hide');
        }else{
          this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                                         
        }
      });
    }




}
