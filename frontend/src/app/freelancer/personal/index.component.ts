import { Component, OnInit } from '@angular/core';
import { PersonalModel } from './personal.model';
import { PaginadorModel } from 'services/paginador.model';
import { Services } from 'services/services';
import { Router, ActivatedRoute,Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { NotificationService } from 'ng2-notify-popup';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {

  title = 'Lista de Freelancer';
  personal:PersonalModel [];
  respuesta:any;
  err:PersonalModel[];
  paginador: PaginadorModel;
  loading;
  
  constructor(private _route: ActivatedRoute,private _router: Router,private _service: Services,private notify:NotificationService) { 
        this.loading=true;
        this.paginador =new PaginadorModel(null,null,null);
    
  }

  ngOnInit() {
    this.getFreelancers();
  }

  getFreelancers(name='', type='', page=1){
  
      this.paginador.name=name;
      this.paginador.type=type;
      this.paginador.page=page;
      
      console.log(this.paginador); this._service.getPostObject('freelancer/registro/',this.paginador).subscribe(response => {
        console.log(response)
        this.personal=response.freelancers;
        this.loading=false;
       });  
  }
}
