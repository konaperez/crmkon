export class PersonalModel {

    constructor(
    	public id:number,
		public nombre:string,
    	public apellido:string,
        public rut:string,
        public telefono:number,
        public correo:string,
        //public _token:string
    )
    {}
}
