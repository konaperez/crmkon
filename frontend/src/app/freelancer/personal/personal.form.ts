import { Component, OnInit } from '@angular/core';
import { PersonalModel } from './personal.model';
import { Services } from 'services/services';
import { Router, ActivatedRoute,Params } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { NotificationService } from 'ng2-notify-popup';

@Component({
  selector: 'app-personal-form',
  templateUrl: './personal.form.html'
})
export class personalForm implements OnInit {

  title = 'Agregar Freelancer';
  in:PersonalModel;
  respuesta:any;
  err:PersonalModel;
  
  constructor( private _service: Services, private _router: Router, private notify:NotificationService ) { 
    this.in = new PersonalModel(0,"","","",0,"");
    this.err = new PersonalModel(0,"","","",0,"");
  }

  ngOnInit() {
  }

  onSubmit() {
  console.log(this.in);
  this._service.getPostObject('freelancer/registro/add',this.in).subscribe(response => {
  
  this.respuesta=response;
  
  console.log("Respuesta ")
  console.log(this.respuesta);
  
  if(this.respuesta.success == 'OK'){
    this.notify.show('PROCESO CORRECTO', { position:'top', duration:'2000', type: 'success' }); 
    this._router.navigate(['freelancer']);
  }else{
    this.err=this.respuesta.errores[0];
    this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
  }
 }); 
    
  }
  
}
