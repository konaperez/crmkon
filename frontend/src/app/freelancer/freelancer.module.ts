import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SelectModule } from 'ng2-select';
import { NgxPaginationModule } from 'ngx-pagination';

import { routing, appRoutingProviders } from './freelancer.routing';

import { PersonalComponent } from './personal/personal';
import { PersonalForm } from './personal/personal.form';



@NgModule({
  declarations: [
  PersonalComponent,
  PersonalForm
  ],
  imports: [
    CommonModule,
    NgxPaginationModule,  
    BrowserModule,
    HttpModule,
    FormsModule,
    routing,
    ReactiveFormsModule,
    SelectModule,
    //OurpalmCKEditorModule
  ],
  providers: [appRoutingProviders]
  
})

export class FreelancerModule { }
