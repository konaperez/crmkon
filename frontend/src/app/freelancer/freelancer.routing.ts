import { NgModule , ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importar componentes
import { PersonalComponent } from './personal/Personal.component';
import { AgregarComponent }    from './personal/agregar.component';
import { Personal }    from './personal/index.component';
import { ViewComponent }    from './personal/view.component';

const freelancerRoutes: Routes = [

    
    {path: 'fr_personal', component: PersonalComponent},
    {path: 'fr_personal'/:type/:id', component: PersonalForm},
    /*

    {path: 'freelancer', component: PersonalComponent,
        children: [
            {path: '', component: IndexComponent },
            {path: 'add', component: AgregarComponent },
           // { path: 'show/:type/:id', component: RoleForm },
           // { path: 'new/:type/:id', component: RoleForm },
        ]
    },

    //*/
];

/*
@NgModule({
    imports: [
        RouterModule.forChild(freelancerRoutes)
    ],
    exports: [ 
        RouterModule
    ]
})

//export class FreelancerRoutingModule {}
//*/
export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(freelancerRoutes);