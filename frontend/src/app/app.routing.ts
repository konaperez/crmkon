import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importar componentes
import { Home } from './home';
import { Login } from './login';
import { Notfound } from './notfound';

const appRoutes: Routes = [
	{path: 'home', component: Home},
	{path: 'login', component: Login},
	{ path: '', component: Home },	
	{path: '**', component: Notfound},

];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);