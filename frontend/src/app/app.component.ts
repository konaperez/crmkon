import { Component,ViewChild,NgZone } from '@angular/core';
import { Services } from 'services/services';
import {GLOBAL} from 'services/global';
import {LoginModel} from './login.model';

declare var $:any;
declare var Pusher: any;

import { NotificationService } from 'ng2-notify-popup';
import { Router } from '@angular/router';
declare var gapi: any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [Services,NotificationService],
})
export class AppComponent {
  info_token=true;
  public respuesta;
  public access=null;
  public info=null;
  public url = GLOBAL.url;
  public urlproject = GLOBAL.urlproject;
  public urlassets = GLOBAL.urlassets;
  public login:LoginModel;
  obj= new Object();
  acceso:any;
  menu:any;

  constructor(private zone: NgZone,private _router: Router,private _service: Services,private notify:NotificationService){
  /*
       this.info_token =  this._service.getAccessSystem();
       this.login=new LoginModel(null,null);

       if(localStorage.getItem('info')!= undefined || localStorage.getItem('info')!= null  ){
         this.info = $.parseJSON( localStorage.getItem('info') );;
       }
<<<<<<< HEAD
       console.log(this.info);
    //*/
=======
    
>>>>>>> 74762ff5c0e1cb0a04d6fefdcd527ad7c6b85b57
  }
  title = 'app';



  ngOnInit() {
    if( this.info_token==true){
      //this.getAccess();
    }

   }

   getAccess(){
    this._service.getAccess('home','home').subscribe(response => {
      this.acceso=response;
      if(this.acceso.success=='OK'){
        this.info_token=true;
        this.getMenu();
      }else{
       this.info_token=false;
       this.salir();
      }
     });
   }
getMenu(){
 let miObjetoInfo= new Object();      
 miObjetoInfo['token'] = localStorage.getItem('token'); 
 this._service.getPostObject('getmodules',miObjetoInfo).subscribe(response => {
   this.menu=response.modules;
   console.log(this.menu);
  });
}


  onSubmit(){
    console.log(this.login);
     this._service.getLogin(this.login).subscribe(response => {
     this.respuesta=response;
     console.log(this.respuesta);
         if(this.respuesta.auth.success == 'OK'){
           localStorage.setItem('info',JSON.stringify(this.respuesta.auth.data));
           localStorage.setItem('token',this.respuesta.auth.token);

           this.access=localStorage.getItem("token");
           this.info=localStorage.getItem("info");
           location.reload();  
           this.info_token=true;
           this.getAccess();
         }else{
           localStorage.setItem('access','0');
           this.access=localStorage.getItem("access");
           this.notify.show('Usuario y/o password incorrectos', { position:'top', duration:'2000', type: 'error' });                          
         }

     },error => {
           this.notify.show('A ocurrido un error al procesar esta acción', { position:'top', duration:'2000', type: 'error' });                          
      });  
          
}


salir(){
  localStorage.removeItem('token')
  localStorage.removeItem('info')
  location.reload();  
}


}
